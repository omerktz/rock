cd sources
python getGroundTruth.py cppcheck
python getGroundTruth.py patl
python getGroundTruth.py pop3
python getGroundTruth.py smtp
python getGroundTruth.py template_dictionary_unittest
python getGroundTruth.py template_regtest
python getGroundTruth.py AntispyComplete
python getGroundTruth.py ShowTraf
python getGroundTruth.py tinyserver
python getGroundTruth.py yafe
python getGroundTruth.py bafprp
python getGroundTruth.py echoparams
python getGroundTruth.py tinyxmlSTL
python getGroundTruth.py gperf
python getGroundTruth.py MidiLib
python getGroundTruth.py tinyxml
python getGroundTruth.py libctemplate
python getGroundTruth.py Analyzer
python getGroundTruth.py Smoothing
python getGroundTruth.py CGridListCtrlEx
mv *.source ..\
cd ..