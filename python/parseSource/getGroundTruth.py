import os
import sys
import re
from pdb import Restart

codeExtensions = ['c','cc','cpp','cxx','h','hh','hpp','hxx']
classDefinitions = ['class', 'interface', 'enum']#, 'struct']

def walkDir(d):
	parents = {}
	parentless = set()
	problems = set()
	for root, subdirs, files in os.walk(d):
		for f in files:
			if '.' in f:
				if f[f.rfind('.')+1:] in codeExtensions:
					readFile(os.path.join(root,f), parents, parentless, problems)
	return (parents, parentless, problems)

def readFile(f,parents,parentless,problems):
	inF = open(f,'r')
	content = ''
	inBigComment = False
	inString = False
	lineCount = 0
	for line in inF:
		lineCount += 1
		line = line.replace('\\\\','\\\\OmerK').replace("'}'","'_'").replace("'{'","'_'").replace("'\"'",'').replace('\\"','_').replace('\\\\OmerK','\\\\').strip()
		if inBigComment:
			if '*/' in line:
				inBigComment = False
				line = line[line.find('*/')+2:]
		if inString:
			if '"' in line:
				inString = False
				line = line[line.find('"')+1:]
		if not inBigComment and not inString:
			while '/*' in line or '//' in line or '"' in line:
				single = line.find('//')
				big = line.find('/*')
				quotes = line.find('"')
				tmp = max([single,big,quotes])+1
				if single == -1:
					single = tmp
				if big == -1:
					big = tmp
				if quotes == -1:
					quotes = tmp
				loc = min([single,big,quotes])
				if single == loc:
					line = line[:single]
				else:
					if big == loc:
						before = line[:line.find('/*')]
						rest = line[line.find('/*')+2:]
						if '*/' in rest:
							rest = rest[rest.find('*/')+2:]
							line = before+rest
						else:
							line = before
							inBigComment = True
					else:
						#if quotes == loc:
						before = line[:line.find('"')]
						rest = line[line.find('"')+1:]
						if '"' in rest:
							rest = rest[rest.find('"')+1:]
							line = before+rest
						else:
							line = before
							inString = True
			line = line.strip()
			if not line.startswith('#include') and not line.startswith('#pragma'):
				content += ' '+line
	scope = {}
	scope['code'] = ''
	scope['kids'] = []
	scope['name'] = ''
	scope['parent'] = None
	while len(content) > 0:
		o = content.find('{')
		e = content.find('}')
		if (e == -1) and (o == -1):
			scope['code'] += content
			content = ''
		else:
			tmp = max([o,e])+1
			if o == -1:
				o = tmp
			if e == -1:
				e = tmp
			loc = min([o,e])
			if o == loc:
				scope['code'] += content[:o]
				content = content[o+1:]
				newScope = {}
				newScope['code'] = ''
				newScope['kids'] = []
				newScope['name'] = scope['code'].split(';')[-1]
				newScope['parent'] = scope
				scope['kids'].append(newScope)
				scope = newScope
			else:
				#if e == loc:
				scope['code'] += content[:e]
				content = content[e+1:]
				scope['code'] = ''
				scope = scope['parent']
				assert scope != None, 'invalid scope '+f
				scope['code'] = ''					
	#assert scope['parent'] == None, 'finished in inner scope '+f
	while scope['parent'] != None:
		scope = scope['parent']
	def handleScope(scope, prefix, parents, parentless, problems):
		name = scope['name'].replace('\t',' ')
		ps = ''
		locs = [m.start() for m in re.finditer(':', name)]
		locs = filter(lambda l: l+1 not in locs and l-1 not in locs, locs)
		if len(locs) > 0:
			loc = max(locs)
			ps = name[loc+1:].strip()
			name = name[:loc].strip()
		if len(filter(lambda x: x in classDefinitions,name.split(' '))) > 0:
			name = name[max(map(lambda x: name.rfind(x), classDefinitions)):]
			name = name[name.find(' '):].strip()
			if len(name) > 0:
				if len(ps) > 0:
					parts = ps.split(',')
					ps = []
					current = ''
					for p in parts:
						current += p
						if '<' in current and '>' not in current:
							current += ','
						else:
							ps.append(current)
							current = ''
					ps = set(filter(lambda z: len(z) > 0, map(lambda y: y[y.find(' ')+1:].strip(),map(lambda x:x.strip(),ps))))
					if prefix+name in parents.keys():
						if not (ps.issuperset(parents[prefix+name]) or ps.issubset(parents[prefix+name])):
							problems.add(prefix+name)
						parents[prefix+name].update(ps)
					else:
						parents[prefix+name] = ps
				parentless.add(prefix+name)
				for k in scope['kids']:
					handleScope(k, prefix+name+'::', parents, parentless, problems)			
		else:
			for k in scope['kids']:
				handleScope(k, prefix, parents, parentless, problems)
	handleScope(scope, '', parents, parentless, problems)

# def readFile(f, parents, parentless, problems):
# 	parenthesis = 0
# 	lastClass = []
# 	inF = open(f,'r')
# 	current = ''
# 	concat = False
# 	lineCount = 0
# 	for line in inF:
# 		lineCount += 1
# 		line = line.strip().replace('\t',' ')
# 		if '//' in line:
# 			line = line[:line.find('//')]
# 		if concat:
# 			current += ' '+line
# 			concat = False
# 		else:
# 			current = line
# 		if '/*' in current:
# 			if '*/' in current:
# 				if current.find('/*') > current.find('*/'):
# 					current = current[current.find('*/')+2:].strip()
# 				while '*/' in current:
# 						current = current[:current.find('/*')]+' '+current[current.find('*/')+2:]
# 				if '/*' in current:
# 					concat = True
# 			else:
# 				concat = True
# 		else:
# 			if '*/' in current:
# 				current = current[current.find('*/')+2:].strip()
# 		if not concat:
# 			parenthesis = max(min(parenthesis,len(lastClass)),0)
# 			if len(filter(lambda d: d in current.split(' '), classDefinitions)) > 0:
# 				if '{' not in current:
# 					concat = True
# 				else:
# 					while len(filter(lambda d: d in current.split(' '), classDefinitions)) > 0:
# 						if '{' in current:
# 							defs = current[:current.find('{')].strip()
# 							closers = defs.count('}')
# 							current = current[current.find('{')+1:].strip()
# 							if ';' in defs:
# 								defs = defs[defs.rfind(';')+1:].strip()
# 							types = filter(lambda d: d in defs.split(' '), classDefinitions)
# 							if len(types) > 1:
# 								print "WARNING: many definition in same line"
# 								print '\t'+defs
# 							else:
# 								if len(defs) > 0:
# 									if len(types) > 0:
# 										defs = defs[defs.find(types[0])+len(types[0]):].strip()
# 										if ':' not in defs:	
# 											name = defs
# 											for i in range(parenthesis,0,-1):
# 												if len(lastClass[i-1].strip())>0:
# 													name = lastClass[i-1]+'::'+name
# 											parentless.add(name)
# 											while len(lastClass) <= parenthesis:
# 												lastClass.append('')
# 											lastClass[parenthesis] = defs
# 										else:
# 											parts = map(lambda l:l.strip(),defs.split(':'))
# 											name = parts[0]
# 											parentless.add(name)
# 											while len(lastClass) <= parenthesis:
# 												lastClass.append('')
# 											lastClass[parenthesis] = name
# 											#name = name[max(name.rfind(' '),name.rfind('\t'))+1:]
# 											for i in range(parenthesis,0,-1):
# 												if len(lastClass[i-1].strip())>0:
# 													name = lastClass[i-1]+'::'+name
# 											ps = set(filter( lambda x:len(x)>0, map(lambda p: p[p.find(' ')+1:].strip(), map(lambda l: l.strip(),parts[1].split(',')))))
# 											if name in parents.keys():
# 												if not (parents[name].issubset(ps) or parents[name].issuperset(ps) or (len(ps) == 0) or (len(parents[name]) == 0)):
# 												#if parents[name] != ps:
# 													print 'ERROR: multiple different definitions of type '+name
# 													problems.add(name)
# 													#sys.exit(0)
# 												parents[name].update(ps)
# 											else:
# 												parents[name] = ps
# 										parenthesis += 1
# 							parenthesis -= closers
# 						else:
# 							concat = True
# 							break;
# 					if len(filter(lambda d: d in current.split(' '), classDefinitions)) > 0:
# 						if '{' not in current:
# 							concat = True
# 			else:
# 				parenthesis += current.count('{')-current.count('}')
   
def getClusters(parents):
	clusters = map(lambda p:[p], parentless)
	for p in parents.keys():
		c = set()
		c.add(p)
		c.update(parents[p])
		rc = []
		for c2 in clusters:
			for x in c:
				if x in c2:
					rc.append(c2)
		for x in rc:
			c.update(x)
		clusters2 = [c]
		for x in clusters:
			if x not in rc:
				clusters2.append(x)
		clusters = clusters2
	return clusters
	
if __name__ == '__main__':
	(parents,parentless,problems) = walkDir(sys.argv[1])
	parentless = filter(lambda p: len(p)>0 and p not in parents.keys(), map(lambda x: x.strip(), parentless))
	f = open(sys.argv[1]+".source",'w')
	for t in sorted(parents.keys()):
		#print t+" :\t"+str(parents[t])[4:-1]
		if t in problems:
			f.write('(problematic)\t')
		f.write(t+" :\t"+str(parents[t])[4:-1]+'\n')
	f.write('\nParentless:\n')
	for t in sorted(parentless):
		f.write(t+'\n')
	clusters = getClusters(parents)
	f.write('\nNumber of clusters: '+str(len(clusters))+'\n')
	for c in sorted(clusters,key=lambda c:len(c),reverse=True):
		tmp = ''
		for x in sorted(c):
			tmp += x+', '
		f.write(str(len(c))+'\t'+tmp[:-2]+'\n')
	f.close()
	