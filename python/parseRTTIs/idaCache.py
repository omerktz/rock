from pymongo import MongoClient
from json import JSONDecoder
import sys
root_filename = sys.argv[1]
db = MongoClient()['OmerK_IdaCache_'+root_filename.replace(' ','_').replace('.','_')]

decoder = JSONDecoder()

BADADDR = 0xFFFFFFFFFFFFFFFFL

def get_root_filename():
    return root_filename

class container():
    def __init__(self,data):
        self.data = data
        self.index = 0;
    def __next__(self):
        if self.index >= len(self.data):
            raise StopIteration()
        else:
            d = self.data[self.index]
            self.index += 1
            return d
    def next(self):
        return self.__next__()
    def __iter__(self):
        return self
    def iter(self):
        return self.__iter__()

class ref():
    def __init__(self, frm):
        self.frm = frm
XrefsTo_cache = {}
def XrefsTo(ea):
    if ea not in XrefsTo_cache.keys():
        try:
            XrefsTo_cache[ea] = map(lambda x: ref(int(x['frm'],16)),decoder.decode(db.XrefsTo.find({'ea':str(ea)}).distinct('target')[0]))
        except IndexError:
            XrefsTo_cache[ea] = []
    return XrefsTo_cache[ea]

class ref2():
    def __init__(self, to):
        self.to = to
XrefsFrom_cache = {}
def XrefsFrom(ea):
    if ea not in XrefsFrom_cache.keys():
        try:
            XrefsFrom_cache[ea] = map(lambda x: ref2(int(x['to'],16)),decoder.decode(db.XrefsFrom.find({'ea':str(ea)}).distinct('target')[0]))
        except IndexError:
            XrefsFrom_cache[ea] = []
    return container(XrefsFrom_cache[ea])

DataRefsTo_cache = {}
def DataRefsTo(ea):
    if ea not in DataRefsTo_cache.keys():
        try:
            DataRefsTo_cache[ea] = map(lambda x: int(x,16),decoder.decode(db.DataRefsTo.find({'ea':str(ea)}).distinct('target')[0]))
        except IndexError:
            DataRefsTo_cache[ea] = []
    return DataRefsTo_cache[ea]

DataRefsFrom_cache = {}    
def DataRefsFrom(ea):
    if ea not in DataRefsFrom_cache.keys():
        try:
            DataRefsFrom_cache[ea] = map(lambda x: int(x,16),decoder.decode(db.DataRefsFrom.find({'ea':str(ea)}).distinct('target')[0]))
        except IndexError:
            DataRefsFrom_cache[ea] = []
    return DataRefsFrom_cache[ea]

GetMnem_cache = {}
def GetMnem(ea):
    if ea not in GetMnem_cache.keys():
        try:
            GetMnem_cache[ea] = db.GetMnem.find({'ea':str(ea)}).distinct('mnem')[0]
        except IndexError:
            GetMnem_cache[ea] = ''
    return GetMnem_cache[ea]

GetOpnd_cache = {}
def GetOpnd(ea,n):
    if (ea,n) not in GetOpnd_cache.keys():
        try:
            GetOpnd_cache[(ea,n)] = db.GetOpnd.find({'ea':str(ea),'n':n}).distinct('opnd')[0]
        except IndexError:
            GetOpnd_cache[(ea,n)] = ''
    return GetOpnd_cache[(ea,n)]

Demangle_cache = {}
def Demangle(s,n):
    if (s,n) not in Demangle_cache.keys():
        try:
            Demangle_cache[(s,n)] = db.Demangle.find({'s':s}).distinct('d')[0]
        except IndexError:
            Demangle_cache[(s,n)] = None
    return Demangle_cache[(s,n)]

LocByName_cache = {}
def LocByName(name):
    if name not in LocByName_cache.keys():
        try:
            LocByName_cache[name] = int(db.LocByName.find({'name':name}).distinct('loc')[0],16)    
        except IndexError:
            LocByName_cache[name] = BADADDR
    return LocByName_cache[name]

ItemSize_cache = {}
def ItemSize(ea):
    if ea not in ItemSize_cache.keys():
        try:
            ItemSize_cache[ea] = int(db.ItemSize.find({'ea':str(ea)}).distinct('size')[0],16)
        except IndexError:
            ItemSize_cache[ea] = 0
    return ItemSize_cache[ea]

GetFunctionName_cache = {}
def GetFunctionName(ea):
    if ea not in GetFunctionName_cache.keys():
        try:
            GetFunctionName_cache[ea] = db.GetFunctionName.find({'ea':str(ea)}).distinct('name')[0]
        except IndexError:
            GetFunctionName_cache[ea] = ''
    return GetFunctionName_cache[ea]

class func():
    def __init__(self,f):
        self.startEA = int(f['startEA'],16)
        self.endEA = int(f['endEA'],16)
get_func_cache = {}
def get_func(ea):
    if ea not in get_func_cache.keys():
        try:
            tmp = db.get_func.find({'ea':str(ea)}).distinct('func')[0]
            if tmp:
                tmp = func(decoder.decode(tmp))
            get_func_cache[ea] = tmp
        except IndexError:
            get_func_cache[ea] = None
    return get_func_cache[ea]

class flow():
    class bb():
        def __init__(self,b):
            self.id = int(b['id'],16)
            self.startEA = int(b['startEA'],16)
            self.endEA = int(b['endEA'],16)
            self.succ = map(lambda x:int(x,16),b['succs'])
            self.pred = map(lambda x:int(x,16),b['preds'])
        def fix(self,bbs):
            self.succ = map(lambda s: bbs[s],self.succ)
            self.pred = map(lambda p: bbs[p],self.pred)
        def succs(self):
            return self.succ
        def preds(self):
            return self.pred
    def __init__(self, bbs):
        self.bbs = map(lambda x: self.bb(x),bbs)
        tmp = {}
        for b in self.bbs:
            tmp[b.startEA] = b
        for b in self.bbs:
            b.fix(tmp)
        self.size = len(self.bbs)
    def __getitem__(self,index):
        return self.bbs[index] 
FlowChart_cache = {}
def FlowChart(f):
    if f not in FlowChart_cache.keys():
        try:
            FlowChart_cache[f] = flow(decoder.decode(db.FlowChart.find({'ea':str(f.startEA)}).distinct('flow')[0]))
        except IndexError:
            FlowChart_cache[f] = None
    return FlowChart_cache[f]

get_imagebase_cache = None
try:
    get_imagebase_cache = int(db.get_imagebase.find().distinct('base')[0],16)
except IndexError:
    get_imagebase_cache = 0
def get_imagebase():
    return get_imagebase_cache

Functions_cache = None
try:
    Functions_cache = map(lambda x: int(x,16),decoder.decode(db.Functions.find().distinct('functions')[0]))
except IndexError:
    Functions_cache = []
def Functions():
    return Functions_cache

Name_cache = {}
def Name(ea):
    if ea not in Name_cache.keys():
        try:
            Name_cache[ea] = db.Name.find({'ea':str(ea)}).distinct('name')[0]
        except IndexError:
            Name_cache[ea] = ''
    return Name_cache[ea]

NextHead_cache = {}
def NextHead(ea):
    if ea not in NextHead_cache.keys():
        try:
            NextHead_cache[ea] = int(db.NextHead.find({'ea':str(ea)}).distinct('head')[0],16)
        except IndexError:
            NextHead_cache[ea] = BADADDR
    return NextHead_cache[ea]

PrevHead_cache = {}
def PrevHead(ea):
    if ea not in PrevHead_cache.keys():
        try:
            PrevHead_cache[ea] = int(db.PrevHead.find({'ea':str(ea)}).distinct('head')[0],16)
        except IndexError:
            PrevHead_cache[ea] = BADADDR
    return PrevHead_cache[ea]

GetFrame_cache = {}
def GetFrame(ea):
    if ea not in GetFrame_cache.keys():
        try:
            tmp = db.GetFrame.find({'ea':str(ea)}).distinct('frame')[0]
            if tmp:
                tmp = int(tmp,16)
            GetFrame_cache[ea] = tmp
        except IndexError:
            GetFrame_cache[ea] = None
    return GetFrame_cache[ea]

GetMemberName_cache = {}
def GetMemberName(frame,member):
    if (frame,member) not in GetMemberName_cache.keys():
        try:
            GetMemberName_cache[(frame,member)] = db.GetMemberName.find({'frame':str(frame),'member':str(member)}).distinct('name')[0]
        except IndexError:
            GetMemberName_cache[(frame,member)] = None
    return GetMemberName_cache[(frame,member)]

GetMemberSize_cache = {}
def GetMemberSize(frame,member):
    if (frame,member) not in GetMemberSize_cache.keys():
        try:
            GetMemberSize_cache[(frame,member)] = int(db.GetMemberSize.find({'frame':str(frame),'member':str(member)}).distinct('size')[0],16)
        except IndexError:
            GetMemberSize_cache[(frame,member)] = None
    return GetMemberSize_cache[(frame,member)]

GetManyBytes_cache = {}
def GetManyBytes(ea,n):
    if (ea,n) not in GetManyBytes_cache.keys():
        try:
            tmp = db.GetManyBytes.find({'ea':str(ea)}).distinct('bytes')[0]
            if tmp:
                tmp = ''.join(map(lambda c:chr(c), map(lambda x:int(x,16), decoder.decode(tmp))))
            GetManyBytes_cache[(ea,n)] = tmp
        except IndexError:
            GetManyBytes_cache[(ea,n)] = None
    return GetManyBytes_cache[(ea,n)]

GetFlags_cache = {}
def GetFlags(ea):
    if ea not in GetFlags_cache.keys():
        try:
            GetFlags_cache[ea] = int(db.GetFlags.find({'ea':str(ea)}).distinct('flags')[0],16)
        except IndexError:
            GetFlags_cache[ea] = 0
    return GetFlags_cache[ea]

isData_cache = {}
def isData(flags):
    if flags not in isData_cache.keys():
        try:
            isData_cache[flags] = db.isData.find({'flags':str(flags)}).distinct('data')[0]
        except IndexError:
            isData_cache[flags] = False
    return isData_cache[flags]

GetFrameArgsSize_cache = {}
def GetFrameArgsSize(ea):
    if ea not in GetFrameArgsSize_cache.keys():
        try:
            GetFrameArgsSize_cache[ea] = int(db.GetFrameArgsSize.find({'ea':str(ea)}).distinct('size')[0],16)
        except IndexError:
            GetFrameArgsSize_cache[ea] = BADADDR
    return GetFrameArgsSize_cache[ea]

GetFirstMember_cache = {}
def GetFirstMember(frame):
    if frame not in GetFirstMember_cache.keys():
        try:
            GetFirstMember_cache[frame] = int(db.GetFirstMember.find({'frame':str(frame)}).distinct('member')[0],16)
        except IndexError:
            GetFirstMember_cache[frame] = -1
    return GetFirstMember_cache[frame]

GetLastMember_cache = {}
def GetLastMember(frame):
    if frame not in GetLastMember_cache.keys():
        try:
            GetLastMember_cache[frame] = int(db.GetLastMember.find({'frame':str(frame)}).distinct('member')[0],16)
        except IndexError:
            GetLastMember_cache[frame] = -1
    return GetLastMember_cache[frame]

GetStrucNextOff_cache = {}
def GetStrucNextOff(frame,member):
    if (frame,member) not in GetStrucNextOff_cache.keys():
        try:
            GetStrucNextOff_cache[(frame,member)] = int(db.GetStrucNextOff.find({'frame':str(frame),'member':str(member)}).distinct('next')[0],16)
        except IndexError:
            GetStrucNextOff_cache[(frame,member)] = -1
    return GetStrucNextOff_cache[(frame,member)]

GetType_cache = {}
def GetType(ea):
    if ea not in GetType_cache.keys():
        try:
            GetType_cache[ea] = db.GetType.find({'ea':str(ea)}).distinct('type')[0]
        except IndexError:
            GetType_cache[ea] = None
    return GetType_cache[ea]

### mocks ###
def autoWait():
    return

def OpHex(ea,n):
    return

def OpOff(ea,n,i):
    return

def ScreenEA():
    return BADADDR

def qexit(n):
    return