import sys
from idaCache import *

class VirtualTableFinder():

	def get_data_refs(self,ea):
		from_refs  = [x for x in DataRefsFrom(ea)]
		to_refs   = [x for x in DataRefsTo(ea)]

		from_ref = None
		if from_refs:
			from_ref = from_refs[0]
		return (from_ref, to_refs)

	virtual_tables = None
	def find_virtual_tables(self):
		def isVTassigned(to_refs,ea):
			def parseOpnd(ea):
				OpHex(ea,1)
				o = GetOpnd(ea,1)
				OpOff(ea,1,0)
				if o.endswith('h'):
					o=o[:-1]
				if o.startswith('0x'):
					o=o[2:]
				try:
					o=long(o,16)
				except ValueError:
					pass
				return o
			if sys.argv[1].endswith('.exe'):
				return len(filter(lambda x:GetMnem(x)=='mov' and parseOpnd(x)==ea,to_refs))!=0
			return True
		if not VirtualTableFinder.virtual_tables:
			virtual_tables = {}
			# enumerate the entire data base and look for possible vtables.
			curr_ea = get_imagebase()
			while curr_ea != BADADDR:
				# get the head flags
				curr_flags = GetFlags(curr_ea)
				# check if its data or code.
				if isData(curr_flags):
					# check if this location has code references if not its not interesting
					from_ref, to_refs = self.get_data_refs(curr_ea)
					#  if it is referenced and it references a function it might be a virtual table
					if from_ref and to_refs:
						if isVTassigned(to_refs,curr_ea):
							vfunctions = []
							vtable_ea = curr_ea
							# count the functions
							while curr_ea and from_ref:
								vfunctions.append(from_ref)
								# There should be no references to the middle of a virtual table
								curr_size = ItemSize(curr_ea)
								curr_ea += curr_size
								from_ref, to_refs = self.get_data_refs(curr_ea)
								if to_refs:
									# go back as we are about to advance
									curr_ea -= curr_size
									break
							# create the virtual table
							if len(vfunctions) > 0:
								virtual_tables[vtable_ea] = vfunctions


				# advance to the next head
				curr_ea = NextHead(curr_ea)

			# remove vtable that with first entry pointing to another vtable, probably RTTI or some other shit like that
			vteas = virtual_tables.keys()
			remove = set()
			for vt in virtual_tables.keys():
				if virtual_tables[vt][0] in vteas:
					remove.add(vt)
			for vt in remove:
				del virtual_tables[vt]

			VirtualTableFinder.virtual_tables = virtual_tables.keys()

		return VirtualTableFinder.virtual_tables

def vtables_main():
	autoWait()
	print 'Started '+sys.argv[1]
	vtables = VirtualTableFinder().find_virtual_tables()
	hierarchy = {}
	rtti = {}
	addressesDir = "addresses/"
	unstrippedNames = {}
	fUnstrippedNames = open(addressesDir+sys.argv[1]+'.addresses.unstripped','r')
	line = fUnstrippedNames.readline()
	while line != '':
		s = line.split('\t')
		unstrippedNames[s[1].strip()] = s[0].strip()
		line = fUnstrippedNames.readline()
	fUnstrippedNames.close()
	for vtable in vtables:
		try:
			curr = XrefsFrom(NextHead(XrefsFrom(NextHead(NextHead(XrefsFrom(PrevHead(vtable)).next().to))).next().to)).next().to
			rttis = [XrefsFrom(curr).next().to]
		except StopIteration:
			continue
		curr = NextHead(curr)
		while len([x for x in XrefsTo(curr)]) == 0:
			targets = [x.to for x in XrefsFrom(curr)]
			if len(targets) > 0:
				rttis.append(targets[0])
			curr = NextHead(curr)
		vtable = hex(vtable)
		if vtable in unstrippedNames.keys():
			vtable = unstrippedNames[vtable]
		rtti[rttis[0]] = vtable
		hierarchy[vtable] = rttis[1:]
	for v in hierarchy.keys():
		hierarchy[v] = map(lambda x: rtti[x] if x in rtti.keys() else hex(x), hierarchy[v])
	f = open(sys.argv[1]+'.rtti','w')
	f.write("All:\n")
	for ea in hierarchy.keys():
		f.write(ea+" : "+str(hierarchy[ea])+'\n')
	f.write("\nReduced:\n")
	# keep only vtables found in the binary
	for vtable in hierarchy.keys():
		hierarchy[vtable] = filter(lambda x: x in hierarchy.keys(), hierarchy[vtable])
	remove = set()
	for vtable in hierarchy.keys():
		if len(hierarchy[vtable]) == 0:
			remove.add(vtable)
	for x in remove:
		del hierarchy[x]
	for ea in hierarchy.keys():
		f.write(ea+" : "+str(hierarchy[ea])+'\n')
	f.close()
	print 'Done!'

if __name__ == '__main__':
	vtables_main()