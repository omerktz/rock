import os
from re import match

from FunctionProvider import Function
from idaCache import *
from vtables import VirtualTableFinder
from DataAnalyzer import *
from State import State

if os.environ.get('OMERK_PROFILING'):
    from multiprocessingMock import Pool
else:
    from multiprocessing import Pool

def getFunctionStartAddress(ea):
    f = get_func(ea)
    if f:
        return f.startEA
    else:
        return ea

dataanalyzers = {}
def getDataAnalyzer(f):
    f = getFunctionStartAddress(f)
    global dataanalyzers
    if f not in dataanalyzers.keys():
        from DataAnalyzer import DataAnalyzer
        da = DataAnalyzer(f)
        da.analyzeFunction()
        dataanalyzers[f] = da
    return dataanalyzers[f]

def getPossibleConstructors(vt):
    return set([r for r in DataRefsTo(vt)])

def filterConstructors(r,v):
    def getLatestMemValue(state, expr):
        while len(set(state.memory.keys()).intersection(expr)) == 0:
            if state.previous:
                state = state.previous[0]
            else:
                return None
        return map(lambda x: state.memory[x], expr)
    def parseNum(n):
        hex = False
        if n.endswith('h'):
            n = n[:-1]
            hex = True
        if n.startswith('0x'):
            n = n[2:]
            hex = True
        try:
            if hex:
                return long(n,16)
            else:
                return long(n,10)
        except ValueError:
            return None
    try:
        func = get_func(getFunctionStartAddress(r))
        if func:
            f = Function(r)
            states = getDataAnalyzer(f).getStatesPerLine()
            prevLines = filter(lambda x: long(x) == PrevHead(r), states.keys())
            if len(prevLines) > 0:
                state = states[prevLines.pop()]
            else:
                state = [State(r)]
            opnd = GetOpnd(r, 0)
            if match('^d?word ptr ', opnd):
                opnd = opnd[opnd.find('ptr ') + 4:]
            if match('^[scdefg]s\:', opnd):
                opnd = opnd[3:]
            expr = map(lambda s: inlineValues(opnd, s)[1:-1], state)
            last = filter(lambda x: GetMnem(long(x)) in ['ret', 'retn'], states.keys())
            lastStates = []
            for x in last:
                lastStates.extend(states[x])
            memVals = map(lambda s: getLatestMemValue(s, expr), lastStates)
            vals = []
            for m in memVals:
                if m:
                    vals.extend(m)
            vals = filter(lambda n:n, map(lambda n:parseNum(n),vals))
            return v in vals
    except:
        return False

def getCalls(ea):
    def clearOpnd(o):
        if match('^[scdefg]s\:',o):
            o = o[3:]
        if match('0x[0-9a-fA-F]+',o):
            try:
                o = long(o,16)
            except ValueError:
                o = 0xffffffffffffffff
        else:
            o = LocByName(o)
        return o
    calls = set()
    func = get_func(ea)
    if func:
        f = Function(ea)
        states = getDataAnalyzer(f).getStatesPerLine()
        prevLines = filter(lambda x: long(x)==PrevHead(ea),states.keys())
        if len(prevLines) == 0:
          return set()
        state = states[prevLines.pop()] 
        opnd = GetOpnd(ea,0)
        if match('^d?word ptr ',opnd):
          opnd = opnd[opnd.find('ptr ')+4:]
        if match('^[scdefg]s\:',opnd):
            opnd = opnd[3:]
        expr = map(lambda s: inlineValues(opnd, s), state)
        if len(filter(lambda e: not e.startswith('[') or not e.endswith(']'), expr)) > 0:
            return set()
        expr = map(lambda e:e[1:-1], expr)
        lines = filter(lambda l: len(set(expr).intersection(map(lambda s: s.registers['ecx'], states[l])))>0,states.keys())
        lines = filter(lambda l: l <= func.endEA,map(lambda l: NextHead(long(l)),lines))
        for l in lines:
            if GetMnem(l).lower() in ['call','jmp']:
                calls.add(getFunctionStartAddress(clearOpnd(GetOpnd(l,0))))
    check = set()
    check.update(calls)
    toAdd = set()
    while len(check) > 0:
        t = check.pop()
        if get_func(t):
            f = Function(t)
            b = f.getInitialBlock()
            c = f.getBlockCommands(b)[0]
            if c.startswith('call ') or c.startswith('jmp '):
                target = getFunctionStartAddress(clearOpnd(c[c.find(' ')+1:]))
                toAdd.add(target)
                check.add(target)               
    calls.update(toAdd)
    return calls

def getAllCallsFromFuncs(fs):
    funcCalls = map(getCalls,fs)
    calls = set()
    for cs in funcCalls:
        calls.update(cs)
    return calls

def getPossibleParents(vt,calls,conss):
    parents = []
    for otherVt in conss.keys():
        if vt != otherVt:
            if len(conss[otherVt].intersection(calls)) > 0:
                parents.append(str(otherVt))
    return parents

def getRelated(vt,vts):
    vfuncs = map(lambda f:f.ea, vts[vt].functions)
    numVfuncs = len(vfuncs)
    related = {}
    for otherVt in vts.keys():
        if vt != otherVt:
            otherVfuncs = map(lambda f:f.ea, vts[otherVt].functions)
            indexes = filter(lambda i: otherVfuncs[i] == vfuncs[i],range(min(numVfuncs,len(otherVfuncs))))
            if len(indexes) > 0:
                related[str(otherVt)] = indexes
    return related

def isImplemented(f):
    ea = f
    while GetMnem(ea) == 'jmp':
        targets = [x.to for x in XrefsFrom(ea)]
        if len(targets) > 0:
            ea = targets[0]
        else:
            return False
    return len(GetMnem(ea).strip()) > 0

def getVirtualFuncs(vt,vts):
    vfuncs = vts[vt].functions
    virtuals = filter(lambda i: not isImplemented(vfuncs[i].ea),range(len(vfuncs)))
    return (virtuals, len(virtuals)/float(len(vfuncs)))


def collectData():
    autoWait()
    vts = VirtualTableFinder().find_virtual_tables()
    from FindClassSize import TypeRuler
    TypeRuler().findClassSizes(vts.values())
    inits = dict(map(lambda v: (v,getPossibleConstructors(v)), vts.keys()))
    conss = dict(map(lambda v: (v,set(map(lambda r: getFunctionStartAddress(r),filter(lambda r: filterConstructors(r,v), inits[v])))), inits.keys()))
    calls = dict(map(lambda v: (v,getAllCallsFromFuncs(inits[v])), inits.keys()))
    parents = dict(filter(lambda (x,y): len(y) > 0, map(lambda v: (str(v),getPossibleParents(v,calls[v],conss)), vts.keys())))
    related = dict(filter(lambda (x,y): len(y) > 0, map(lambda v: (str(v),getRelated(v,vts)),vts.keys())))
    virtual = dict(filter(lambda (x,y): len(y[0]) > 0, map(lambda v: (str(v),getVirtualFuncs(v,vts)),vts.keys())))
    return (parents,related,virtual,vts)
    
if __name__ == '__main__':
    (parents,related,virtual,vts) = collectData()
    f = open(sys.argv[1]+".analysis",'w')
    for x in vts.keys():
        n = str(x)
        f.write(n+'\n')
        f.write('\tVTable size:\t'+str(len(vts[x].functions))+'\n')
        f.write('\tAllocated size:\t'+str(vts[x].alloc)+'\t('+str(vts[x].foundAllocation)+')\n')
        f.write('\tParents:\n')
        if n in parents.keys():
          f.write('\t\t'+str(parents[n])+'\n')
        f.write('\tRelated:\n')
        if n in related.keys():
          for y in related[n].keys():
            f.write('\t\t'+str(y)+'\t'+str(related[n][y])+'\n')
        f.write('\tVirtual:\n')
        if n in virtual.keys():
          f.write('\t\t'+str(virtual[n][1])+' '+str(virtual[n][0])+'\n')
        f.write('\n')
    f.close()
    
    clusters = {}
    for x in related.keys():
        cluster = set()
        cluster.add(x)
        cluster.update(related[x].keys())
        for y in related[x].keys():
            if y in clusters.keys():
                cluster.update(clusters[y])
        for c in cluster:
            clusters[c] = cluster
    f = open(sys.argv[1]+".clusters",'w')
    printed = set()
    for c in clusters.keys():
        if c not in printed:
            tmp = ""
            for x in clusters[c]:
                printed.add(x)
                tmp += x+', '
            f.write(tmp[:-2]+'\n\n')
    f.close()
