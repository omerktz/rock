class State:
	regs = ['esi', 'edi', 'ebp', 'esp', 'eax', 'ebx', 'ecx', 'edx']
	reinitIndex = {}
	
	def __init__(self, ea, reinitRegs = True, prevState = []):
		self.registers = {'esi':'ESI', 'edi':'EDI', 'ebp':'EBP', 'esp':'ESP', 'eax':'EAX', 'ebx':'EBX', 'ecx':'ECX', 'edx':'EDX'}
		if reinitRegs:
			self.reinit(ea,0)	
		self.stack = {}
		self.memory = {}
		self.ea = ea
		self.previous = []
		self.topOfArgs = ['ESP']
	
	def __eq__(self,other):
		return (self.registers.__eq__(other.registers) and self.stack.__eq__(other.stack) and self.memory.__eq__(other.memory))
		
	def __ne__(self,other):
		return not self.__eq__(other)
		
	def __str__(self):
		def printDict(d):
			s = ""
			for k in sorted(d.keys()):
				s += str(k) + ":" + str(d[k]) + ","
			s = "{" + s[:-1] + "}"
			return s
		return "{registers:"+printDict(self.registers)+",stack:"+printDict(self.stack)+",memory:"+printDict(self.memory)+"}"
	
	def __hash__(self):
		return hash(self.__str__())
	
	def copy(self,prev=None):
		s = State(self.ea)
		s.stack = self.stack.copy()
		s.memory = self.memory.copy()
		s.registers = self.registers.copy()
		s.topOfArgs = self.topOfArgs[:]
		if prev:
			if type(prev) == list:
				s.previous = prev
			else:
				s.previous = [prev]
		else:
			s.previous = self.previous
		s.ea = self.ea
		return s
	
	def reinit(self,ea,l):
		self.memory = {}
		regsToReinit = ['eax', 'ecx', 'edx']
		if ea not in State.reinitIndex.keys():
			State.reinitIndex[ea] = {}
			State.reinitIndex[ea][0] = 0
		if l not in State.reinitIndex[ea].keys():
			State.reinitIndex[ea][l] = len(State.reinitIndex[ea].keys())
		for r in regsToReinit:
			self.registers[r] = r.upper()+str(State.reinitIndex[ea][l])
			
	# check if state doesn't contain any illegal registers
	def checkState(self):
		for r in self.registers.keys():
			if r not in State.regs:
				return False
		return True
	
	# set value to be used as the top of the arguments pushed to the stack
	def setTopOfArgs(self,val):
		self.topOfArgs = [val] + self.topOfArgs	
	
	# get size of arguments in the stack
	def getLengthOfArgs(self):
		if len(self.topOfArgs) == 0:
			return 0
		from StateUtils import simplifyExpression
		length = simplifyExpression('('+self.topOfArgs[0]+')-('+self.registers['esp']+')')
		try:
			return int(length[:-1],16)
		except ValueError:
			#print 'Length of arguments could not be determined!'
			return 0
		
	def updateEA(self,ea):
		self.ea = ea