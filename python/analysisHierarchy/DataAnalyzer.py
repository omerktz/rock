from re import match

from StateUtils import inlineValues
from idaCache import *


class DataAnalyzer:

	def __init__(self,func):
		self.function = func
		self.statesPerLine = {}
		self.calls = {}
		self.allCalls = []
		self.objectInCall = {}
		#self.firstArg = True
		self.fields = None
		self.enders = set()
		self.starters = set()
		#self.hasVirtualCalls = False
		self.firstCommand = True
		
	def updateRegisterValue(self,s,r,v):
		s.registers[r]=inlineValues(v,s)
		
	def prepareStackAddress(self,o,s):
		# add offset to current esp and simplify
		return inlineValues('esp+'+o,s).replace(' ','')
	def updateStackAtOffset(self,s,o,v):
		self.updateStackAtOffsetFixedAddress(s,self.prepareStackAddress(o,s),v)
	def updateStackAtOffsetFixedAddress(self,s,o,v):
		val = inlineValues(v,s)
		s.stack[o]=val
		#if self.firstArg:
		#	if not match('^((E[ACD]X0)|(EBX)|(E[SD]I)|(E[SBI]P))$',val):
		#		# as long as we're pushing initial values on to the stack, these are probably not arguments
		#		self.firstArg = False
		#	else:
		#		s.setTopOfArgs(inlineValues('esp',s))
		if match('^((EBX)|(E[SD]I)|(E[SB]P))$',val):
			s.setTopOfArgs(inlineValues('esp',s))
	def removeStackAtEsp(self,s):
		address = self.prepareStackAddress('0',s)
		if address != 'ESP':
			if address in s.topOfArgs:
				s.topOfArgs = s.topOfArgs[s.topOfArgs.index(address)+1:]
		# delete value from stack (used for pop etc.)
		try: 
			del s.stack[address]
		except KeyError:
			#print '\tRead from stack without writing first'
			pass
		
	def updateMemoryAtAddress(self,s,a,v):
		self.updateMemoryAtAddressFixedAddress(s,inlineValues(a,s),v)
	def updateMemoryAtAddressFixedAddress(self,s,a,v):
		s.memory[a]=inlineValues(v,s)
	
	def splitArgs(self,a):
		# prepare arguments for use
		def cleanArg(arg):
			arg = arg.strip()
			if match('^(((e?[abcd][hlx])|(e?[bsi]p)|(e?[sd]i)|([scdefg]s))|(-?[0-9]+|-?0x[0-9a-fA-F]+|0-9a-fA-F]+h)|(\[[^\]\[]+\]))$',arg):
				# arg is value or register and should not be changed
				return arg
			if match('^(large )?(d?word ptr )?[scdefg]s\:',arg):
				# remove prefix
				return '['+arg[arg.find(':')+1:]+']'
			if match('^d?word ptr \[',arg):
				# remove prefix
				return arg[arg.find('ptr ')+4:]
			if match('^d?word ptr ',arg) or match('^byte ptr ',arg):
				#remove prefix
				return '['+arg[arg.find('ptr ')+4:]+']'
			#probably an offset in memory (handle as address)
			return '['+arg+']'
		args = [a,'']
		if a.find(',') != -1:
			args = a.split(',')
		for i in xrange(len(args)):
			args[i] = cleanArg(args[i])
		return args
		
	def updateTargetWithSource(self,states,target,source):
		# decide if target is register, stack or memory and update accordingly
		if match('^[abcd][hlx]$',target) or match('^[sd]i$',target) or match('^[bis]p$',target):
			#print '\tTarget is:\''+target+'\'. Unlikely to be used in any relevant computation. Command Skipped!'
			if target[1] in ['h','l']:
				target = target[0]+'x'
			for s in states:
				self.updateRegisterValue(s,'e'+target,'Unknown')
			return
		if target.startswith('['):
			for s in states:
				#assign to buffer
				targetS = inlineValues(target[1:-1],s).replace(' ','')
				if targetS.startswith('ESP') or targetS.startswith(s.registers['esp'].replace(' ','')) or 'esp' in target:
					self.updateStackAtOffsetFixedAddress(s,targetS,source)
				else:
					
					self.updateMemoryAtAddressFixedAddress(s,targetS,source)
		else:
			#assign to register
			for s in states:
				self.updateRegisterValue(s,target,source)
		
	def handleCall(self,states,args,lineId):
		from func_utils import get_function_calling_convention,get_function_args_size
		# handle call (from either call or jump table)
		def cleanTargetForCall(arg):
			arg = arg.strip()
			matched = match('^((large )?(dword ptr )?([scdefg]s\:)?)',arg)
			if matched:
				arg = arg[len(matched.group(0)):]
			#probably an offset in memory (handle as address)
			return arg
		self.allCalls.append(lineId)
		for s in states:
			callTarget = inlineValues(cleanTargetForCall(args),s)
			#print '\ttarget is ' + callTarget
			targetEA = LocByName(callTarget)
			# determine if need to fix stack after call
			targetCallConvention = get_function_calling_convention(targetEA)
			byteslength = 0
			byteslength = get_function_args_size(targetEA)
			if match('.*@[0-9]+$',callTarget):
				byteslength = int(callTarget[callTarget.rfind('@')+1:])
				targetCallConvention = 'needsCorrection'
			# determine if call is to virtual table
			matched = match('\[\[(.*)\](\+([0-9]+|[0-9a-f]+h))?\]',callTarget)
			if matched:
				offset = matched.group(3)
				if offset == None:
					offset = 0
				else:
					if offset.endswith('h'):
						offset = int(offset[:-1],16)
					else:
						offset = int(offset)
				actualCallTarget = matched.group(1)
				#comment = ''
				# check if object has special meaning
				#if match('\[ESP\+[0-9a-fA-F]+h\]',actualCallTarget):
				#	def replaceWithArgString(a):
				#		for m in findall('\[ESP\+[0-9a-fA-F]+h\]',actualCallTarget):
				#			num = int(actualCallTarget[4:-2],16)/4
				#			if num == 0:
				#				a = a.replace(m,'RET_ADDRESS')
				#			else:
				#				a = a.replace(m,'ARG' + str(num))
				#		return a
				#	comment = ' (' + replaceWithArgString(actualCallTarget) + ')'
				#print '\t\tVirtual Table Call!\t object is: ' + actualCallTarget + comment + '\t offset is: ' + str(offset)
				#self.hasVirtualCalls = True
				lineIdentifier = hex(lineId.b.id)+':'+actualCallTarget
				if lineId not in self.calls.keys():
					self.calls[lineId] = []
				self.calls[lineId].append({'target':lineIdentifier, 'offset':offset, 'args':s.getLengthOfArgs()})
				if lineId not in self.objectInCall.keys():
					self.objectInCall[lineId] = set()
				self.objectInCall[lineId].add(lineIdentifier+'.'+str(offset))
			# correct stack if needed
			if targetCallConvention in ['needsCorrection','__stdcall','__fastcall']:
				#self.updateRegisterValue(s,'esp','esp+'+str(bytes))
				b = 0;
				while b < byteslength:
					self.removeStackAtEsp(s)
					self.updateRegisterValue(s,'esp','esp+4')
					b += 4
			# after calling a function, some register values are unknown
			s.reinit(self.function.ea,long(lineId))
		
	def parseCommand(self,c,states,lineId):
		#update state according to each command
		if len(c.strip()) == 0:
			return states
		if c.find(' ') != -1:
			mnemonic = c[:c.find(' ')]
			args = c[c.find(' ')+1:]
		else:
			mnemonic = c
			args = ''
		(target,source) = self.splitArgs(args)
		if mnemonic == 'mov':
			self.updateTargetWithSource(states,target,source)
		elif mnemonic == 'movs':
			self.updateTargetWithSource(states,'[edi]','[esi]')
		elif mnemonic == 'lea':
			if not (source.startswith('[') and source.endswith(']')):
				raise ValueError('lea value: ' + source)
			self.updateTargetWithSource(states,target,source[1:-1])
		elif mnemonic == 'push':
			for s in states:
				self.updateRegisterValue(s,'esp','esp-4')
				self.updateStackAtOffset(s,'0',target)
		elif mnemonic == 'pop':
			for s in states:
				if target.startswith('['):
					self.updateMemoryAtAddress(s,target[1:-1],'[esp]')
				else:
					self.updateRegisterValue(s,target,'[esp]')
				self.removeStackAtEsp(s)
				self.updateRegisterValue(s,'esp','esp+4')
		elif mnemonic == 'add':
			if target == 'esp':
				try:
					if source.endswith('h'):
						source = source[:-1]
					byteslength = long(source,16)
					while byteslength >= 4:
						for s in states:
							self.removeStackAtEsp(s)
							self.updateRegisterValue(s,'esp','esp+4')
						byteslength -= 4
					if byteslength > 0:
						for s in states:
							self.updateRegisterValue(s,'esp','esp+'+str(byteslength))
				except ValueError:
					self.updateTargetWithSource(states,target,target+'+'+source)
			else:
				self.updateTargetWithSource(states,target,target+'+'+source)
		elif mnemonic == 'sub':
			self.updateTargetWithSource(states,target,target+'-'+source)
		elif mnemonic == 'sbb':
			self.updateTargetWithSource(states,target,target+'-'+source)
		elif mnemonic == 'stos':
			self.updateTargetWithSource(states,'edi','eax')
		elif mnemonic == 'retn':
			# function ends at ret
			if args != '':
				for s in states:
					self.updateRegisterValue(s,'esp','esp+'+target)			
		elif mnemonic == 'call':
			self.handleCall(states,args,lineId)
		elif mnemonic == 'cmp':
			#print '\tNothing to be done'
			pass
		elif mnemonic == 'sar':
			self.updateTargetWithSource(states,target,target+'/(2^'+source+')')
		elif mnemonic == 'shr':
			self.updateTargetWithSource(states,target,target+'/(2^'+source+')')
		elif mnemonic == 'shl':
			self.updateTargetWithSource(states,target,target+'*(2^'+source+')')
		elif mnemonic == 'test':
			#print '\tNothing to be done'		
			pass
		elif mnemonic == 'xor':
			if target == source:
				self.updateTargetWithSource(states,target,'0')
			else:
				self.updateTargetWithSource(states,target,'BitwiseXor('+target+','+source+')')
		elif mnemonic == 'pushf':
			for s in states:
				self.updateRegisterValue(s,'esp','esp-4')
				self.updateStackAtOffset(s,'0','FLAGS')
		elif mnemonic == 'popf':
			for s in states:
				self.removeStackAtEsp(s)
				self.updateRegisterValue(s,'esp','esp+4')
		elif mnemonic == 'or':
			self.updateTargetWithSource(states,target,'BitwiseOr('+target+','+source+')')
		elif mnemonic == 'and':
			self.updateTargetWithSource(states,target,'BitwiseAnd('+target+','+source+')')
		elif mnemonic == 'cpuid':
			for s in states:
				self.updateRegisterValue(s,'eax','CPUID')
		elif mnemonic == 'not':
			for s in states:
				if target.startswith('['):
					self.updateMemoryAtAddress(s,target[1:-1],'bNot('+target[1:-1]+')')
				else:
					self.updateRegisterValue(s,target,'bNot('+target+')')
		elif mnemonic == 'neg':
			for s in states:
				if target.startswith('['):
					self.updateMemoryAtAddress(s,target[1:-1],'-('+target[1:-1]+')')
				else:
					self.updateRegisterValue(s,target,'-('+target+')')
		elif mnemonic == 'nop':
			pass
		elif mnemonic == 'jmp':
			# sometimes virtual call is implemented using jmp
			possibleCall = False or self.firstCommand
			if not possibleCall:
				try:
					if get_func(long(target[2:],16)):
						if get_func(long(target[2:],16)).startEA != self.function.ea:
							# this jmp is actually a call to another function...
							possibleCall = True
				except ValueError:
					pass
				if not possibleCall:
					for s in states:
						if match('\[\[(.*)\](\+([0-9]+|[0-9a-f]+h))?\]',inlineValues(target,s)):
							possibleCall = True
			if possibleCall:
				self.handleCall(states,args,lineId)
		elif mnemonic == 'int':
			#print '\tNothing to be done'
			pass
		elif mnemonic == 'inc':
			self.updateTargetWithSource(states,target,target+'+1')
		elif mnemonic == 'dec':
			self.updateTargetWithSource(states,target,target+'-1')
		elif mnemonic == 'leave':
			for s in states:
				self.updateRegisterValue(s,'esp','ebp')
				self.updateRegisterValue(s,'ebp','[esp]')
				self.removeStackAtEsp(s)
				self.updateRegisterValue(s,'esp','esp+4')
		elif mnemonic == 'setnz':
			#print '\tNothing to be done'
			pass
		elif mnemonic == 'fninit':
			#print '\t'+mnemonic+' NOT Handled!'
			pass
		elif mnemonic == 'cmpxchg':
			#print '\t'+mnemonic+' NOT Handled!'
			pass
		elif mnemonic == 'movzx':
			self.updateTargetWithSource(states,target,'ZeroExtend('+source+')')
		elif mnemonic == 'xchg':
			if target != source:
				# xchg via register:
				self.updateTargetWithSource(states,'tmp',source)
				self.updateTargetWithSource(states,source,target)
				self.updateTargetWithSource(states,target,'tmp')
				for s in states:
					del s.registers['tmp']
				"""
				# xchg via memory:
				self.updateTargetWithSource(states,'[tmp]',source)
				self.updateTargetWithSource(states,source,target)
				self.updateTargetWithSource(states,target,'[tmp]')
				for s in states:
					del s.memory['tmp']
				"""
		elif mnemonic == 'mul':
			self.updateTargetWithSource(states,'eax','Bottom(eax*'+target+')')
			self.updateTargetWithSource(states,'edx','Top(eax*'+target+')')
		elif mnemonic == 'imul':
			if args.count(',') == 1:
				self.updateTargetWithSource(states,'eax','Bottom(eax*'+target+')')
				self.updateTargetWithSource(states,'edx','Top(eax*'+target+')')
			if args.count(',') == 2:
				self.updateTargetWithSource(states,target,target+'*'+source)
			if args.count(',') == 3:
				operands = self.splitArgs(args)
				self.updateTargetWithSource(states,operands[0],operands[1]+'*'+operands[2])
		else:
			#print "\tUnknown command: " + mnemonic
			pass
		self.firstCommand = False
		return states

	def initialPass(self):
		pass
		
	def updateStatesPerLine(self,_id,s):
		#self.statesPerLine[_id] = s
		if _id not in self.statesPerLine.keys():
			self.statesPerLine[_id] = []
		self.statesPerLine[_id].extend(s)
	
	def mergeStates(self,states):
		def partitionStates(states):
			classes = []
			for s in states:
				found = False
				for c in classes:
					if s.__eq__(c[0]):
						c.append( s )
						found = True
						break
				if not found: # it is in a new class
					classes.append([s])
			return classes
		def getRepresentative(states):
			previous = []
			for s in states:
				previous.extend(s.previous)
			return states[0].copy(previous)
		return map(lambda x:getRepresentative(x),partitionStates(states))
	
	def analyzeFunction(self):
		from State import State
		# perform non-flow-sensitive analysis
		self.__init__(self.function)
		self.initialPass()
		func = self.function
		inStates = {}
		newInStates = {}
		outStates = {}
		initialBlocks = func.getInitialBlocks()
		blocks = []
		for b in initialBlocks:
			newInStates[b] = [State(b.start_addr)]
			blocks.append(b)
		# function starts at initial block
		self.starters = set(map(lambda b:func.getLineId(b,0),initialBlocks))
		# go over all blocks
		while len(blocks) != 0:
			i = 0
			v = blocks.pop(0)
			commands = func.getBlockCommands(v)
			# create work copy of states
			states = newInStates[v]
			#print hex(v.start_addr)[:-1] + ' ' + str(len(states))
			if v not in inStates.keys():
				inStates[v] = []
			inStates[v].extend(states)
			# parse all commands
			lastCommand = None
			for c in commands:
				if len(c) != 0:
					#print '\t'+c
					lineId = func.getLineId(v,i)
					#print lineId
					states = map(lambda s:s.copy(s),states)
					map(lambda s:s.updateEA(long(lineId)),states)
					states = self.mergeStates(self.parseCommand(c, states, lineId))
					lastCommand = lineId
					self.updateStatesPerLine(lineId,states)
					i += int(ItemSize(long(lineId)))
			if lastCommand:
				if GetMnem(long(lastCommand)) in ['retn','jmp']:
					def isInSameFunction(b):
						fIn = get_func(b.source().start_addr)
						fOut = get_func(b.target().start_addr)
						if fIn:
							if fOut:
								return fIn.startEA == fOut.startEA
						return False
					if len(filter(lambda b:isInSameFunction(b),func.getBlockOutEdges(v))) == 0:
						self.enders.add(lastCommand)
			# if new out state created, update successors
			if v not in outStates.keys():
				outStates[v] = []
			outStates[v].extend(states)
			newInStates[v] = []
			for n in func.getBlockSuccessors(v):
				if n not in newInStates.keys():
					newInStates[n] = []
				newStates = filter(lambda s:s not in newInStates[n],states)
				if len(newStates) != 0:
					newInStates[n].extend(newStates)
					if n not in blocks:
						blocks.append(n)

	"""
	def analyzeFunctionByPath(self,path):
		from State import State
		# perform flow-sensitive analysis
		self.__init__(self.function)
		self.initialPass()
		func = self.function
		inStates = {}
		outStates = {}
		blocks = {}
		# create mapping form line ids to blocks
		for b in func.getBlocks():
			blocks[func.getLineId(b,0)] = b
		currentStep = path.next()
		inStates[blocks[currentStep]] = set([State(self.function.ea)])
		# function starts at initial block
		self.starters = set([func.getLineId(func.getInitialBlock(),0)])
		if self.starters[0] != currentStep:
			raise Exception('Incorrect first step in path')
		# go over entire path
		notDone = True
		while notDone:
			# only need to synchronize with path at start of each block (block is deterministic)
			v = blocks[currentStep]
			i = 0
			commands = func.getBlockCommands(v)
			# create work copy of states
			states = inStates[v]
			# parse all commands
			lastCommand = None
			for c in commands:
				if len(c) != 0:
					#print c
					lineId = func.getLineId(v,i)
					states = map(lambda s:s.copy(s),states)
					states = self.parseCommand(c, states, lineId)
					lastCommand = lineId
					self.updateStatesPerLine(lineId,states)
					i += int(ItemSize(long(lineId)))
					try:
						currentStep = path.next()
					except StopIteration:
						notDone = False
			if lastCommand:
				if GetMnem(long(lastCommand)) in ['retn','jmp']:
					def isInSameFunction(b):
						fIn = get_func(b.source().start_addr)
						fOut = get_func(b.target().start_addr)
						if fIn:
							if fOut:
								return fIn.startEA == fOut.startEA
						return False
					if len(filter(lambda b:isInSameFunction(b),func.getBlockOutEdges(v))) == 0:
						self.enders.add(lastCommand)
			# if new out state created, update successors
			if v not in outStates.keys():
				outStates[v] = set()
			newOutStates = set()
			for s in states:
				if s not in outStates[v]:
					outStates[v].add(s)
					newOutStates.add(s)
			if len(newOutStates) != 0:
				for n in func.getBlockSuccessors(v,True):
					if n not in inStates.keys():
						inStates[n] = set()
					for s in newOutStates:
						if s not in inStates[n]:
							inStates[n].add(s)
	"""
	
	"""
	def parseFields(self,additionals):
		# after initial analysis is done, we know all objects in function, find all field accesses to objects
		def parseObjectAndOffset(v):
			matched = match('^\[(.+)\+([0-9a-fA-F]+)h\]$',v)
			if matched:
				return matched.groups()
			matched = match('^\[(.+)\]$',v)
			if matched:
				return matched.groups()
			return None
		def handleValue(val,inORout=True):
			ono = parseObjectAndOffset(val)
			if ono:
				obj = ono[0]
				offset = 0
				if len(ono) > 1:
					offset = int(ono[1],16)
				if obj in objects:
					if obj not in self.fields.keys():
						self.fields[obj] = set()
					self.fields[obj].add((l,offset,inORout))
		# interesting objects in function are both virtual objects and objects with known deterministic virtual types
		self.fields = {}
		self.additionals = set(additionals)
		objects = self.getAllObjects()
		for l in self.statesPerLine.keys():
			states = self.statesPerLine[l]
			# only record field access if this is the first line it appears (only if it didn't appear in previous state)
			for s in states:
				if match('^\[.+\]$',GetOpnd(long(l),1)):
					for r in s.registers.keys():
						for p in s.previous:
							if s.registers[r] != p.registers[r]:
								handleValue(s.registers[r])
								break
						if len(s.previous)==0:
							handleValue(s.registers[r])
				if GetMnem(long(l))!='mov' or match('^\[.+\]$',GetOpnd(long(l),1)):
					for o in s.stack.keys():
						for p in s.previous:
							if o not in p.stack.keys() or s.stack[o] != p.stack[o]:
								handleValue(s.stack[o])
								break
						if len(s.previous)==0:
							handleValue(s.stack[o])
				for m in s.memory.keys():
					checkAddress = (len(s.previous)==0)
					checkValue = checkAddress
					if checkAddress:
						for p in s.previous:
							if m not in p.memory.keys():
								checkAddress = True
								checkValue = True
								break
					if not checkValue:
						for p in s.previous:
							if s.memory[m] != p.memory[m]:
								checkValue = True
								break
					if checkAddress:
						handleValue('['+m+']',False)
					if checkValue:
						handleValue(s.memory[m])
	"""
	
	def parseAllFields(self,):
		# after initial analysis is done, we know all objects in function, find all field accesses to objects
		#print 'in parseAll'
		def parseObjectAndOffset(v):
			matched = match('^\[(.+)\+([0-9a-fA-F]+)h\]$',v)
			if matched:
				return matched.groups()
			matched = match('^\[(.+)\]$',v)
			if matched:
				return matched.groups()
			return None
		def handleValue(val,inORout=True):
			if (not inORout) or match('^\[.+\]$', val):
				ono = parseObjectAndOffset(val)
				if ono:
					obj = ono[0]
					offset = 0
					if len(ono) > 1:
						offset = int(ono[1],16)
					if obj not in self.fields.keys():
						self.fields[obj] = set()
					self.fields[obj].add((l,offset,inORout))
		# interesting objects in function are both virtual objects and objects with known deterministic virtual types
		self.fields = {}
		for l in self.statesPerLine.keys():
			states = self.statesPerLine[l]
			OpHex(long(l),1)
			opnd = GetOpnd(long(l),1)
			OpOff(long(l),1,0)
			# only record field access if this is the first line it appears (only if it didn't appear in previous state)
			for s in states:
				if match('^\[.+\]$',opnd):
					for r in s.registers.keys():
						for p in s.previous:
							if s.registers[r] != p.registers[r]:
								handleValue(s.registers[r])
								break
						if len(s.previous)==0:
							handleValue(s.registers[r])
				#if GetMnem(long(l))!='mov' or if GetMnem(long(l))!='push' or match('^\[.+\]$',opnd):
				for o in s.stack.keys():
					for p in s.previous:
						if o not in p.stack.keys() or s.stack[o] != p.stack[o]:
							handleValue(s.stack[o])
							break
					if len(s.previous)==0:
						handleValue(s.stack[o])
				for m in s.memory.keys():
					checkAddress = (len(s.previous)==0)
					checkValue = checkAddress
					if not checkAddress:
						for p in s.previous:
							if m not in p.memory.keys():
								checkAddress = True
								checkValue = True
								break
					if not checkValue:
						for p in s.previous:
							if s.memory[m] != p.memory[m]:
								checkValue = True
								break
					if checkAddress:
						handleValue('['+m+']',False)
					if checkValue:
						handleValue(s.memory[m])
	
	"""		
	def getFieldsForObject(self,target):
		from StateUtils import isValidExpr
		# after initial analysis is done, we know all objects in function, find all field accesses to objects
		fields = set()
		def parseObjectAndOffset(v):
			matched = match('^\[(.+)\+([0-9a-fA-F]+)h\]$',v)
			if not matched:
				matched = match('^\[(.+)\]$',v)	
			if matched:
				if isValidExpr(matched.groups()[0]):
					return matched.groups()
			return None
		def handleValue(val,inORout=True):
			ono = parseObjectAndOffset(val)
			if ono:
				obj = ono[0]
				offset = 0
				if len(ono) > 1:
					offset = int(ono[1],16)
				if obj == target:
					fields.add((l,offset,inORout))
		# interesting objects in function are both virtual objects and objects with known deterministic virtual types
		for l in self.statesPerLine.keys():
			states = self.statesPerLine[l]
			# only record field access if this is the first line it appears (only if it didn't appear in previous state)
			for s in states:
				if match('^\[.+\]$',GetOpnd(long(l),1)):
					for r in s.registers.keys():
						for p in s.previous:
							if s.registers[r] != p.registers[r]:
								handleValue(s.registers[r])
								break
						if len(s.previous)==0:
							handleValue(s.registers[r])
				if GetMnem(long(l))!='mov' or match('^\[.+\]$',GetOpnd(long(l),1)):
					for o in s.stack.keys():
						for p in s.previous:
							if o not in p.stack.keys() or s.stack[o] != p.stack[o]:
								handleValue(s.stack[o])
								break
						if len(s.previous)==0:
							handleValue(s.stack[o])
				for m in s.memory.keys():
					checkAddress = (len(s.previous)==0)
					checkValue = checkAddress
					if checkAddress:
						for p in s.previous:
							if m not in p.memory.keys():
								checkAddress = True
								checkValue = True
								break
					if not checkValue:
						for p in s.previous:
							if s.memory[m] != p.memory[m]:
								checkValue = True
								break
					if checkAddress:
						handleValue('['+m+']',False)
					if checkValue:
						handleValue(s.memory[m])
		return fields
	"""
		
	def getFields(self):
		#if not set(additionals).issubset(self.additionals):
		#	self.parsedFields = False
		#	self.fields = {}
		if not self.fields:
			self.parseAllFields()
		return self.fields
	
	"""	
	def getNonVirtualCallTargets(self):
		res = {}
		for c in self.allCalls:
			target = GetOpnd(long(c),0)
			address = LocByName(target)
			if address != 0xffffffffffffffff:
				res[c] = target
		return res
	"""
	
	def printStatesPerLine(self):
		for l in sorted(self.statesPerLine.keys()):
			print str(l) + ':'
			for s in self.statesPerLine[l]:
				print "\t" + str(s)
				#if not s.checkState():
				#	raise Error('Bad State')
				
	def getStatesPerLine(self):
		return self.statesPerLine
		
	def getCalls(self):
		return self.calls
	
	def getAllCalls(self):
		return self.allCalls
	
	def getObjectsInCalls(self):
		return self.objectInCall
		
	def getEntryPoints(self):
		return self.starters
		
	def getExitPoints(self):
		return self.enders
	
	def getExitPointStates(self):
		return map(lambda l: self.statesPerLine[l], filter(lambda l: l in self.enders, self.statesPerLine.keys()))
	
	"""
	def getAllObjects(self):
		from GetKnownTypes import TypeFinder
		objects = set()
		for c in self.calls.keys():
			objects = objects.union(map(lambda x:x['target'],self.calls[c]))
		objects = set(self.additionals).union(set(map(lambda x: x[x.find(':')+1:], objects)))
		objects = objects.union(set(TypeFinder().getKnownTypes(self.function.ea)))
		return objects
	"""
	
if __name__ == '__main__':
	print 'Done!'
