treeScores = {}
forestScores = {}
import os
numFiles = 0
for file in os.listdir('.'):
	if file.endswith('.stats'):
		f = open(file,'r')
		first= True
		for line in f.readlines():
			parts = line.split('\t')
			if first:
				first = False
				baselineForest = float(parts[1][parts[1].find('=')+1:])
				baselineTree = float(parts[2][parts[2].find('=')+1:])
			else:
				tree = float(parts[-1]) - baselineTree
				forest = float(parts[-2]) - baselineForest
				desc = '\\t'.join(parts[1:-2])
				if desc not in treeScores.keys():
					treeScores[desc] = tree
					forestScores[desc] = forest
				else:
					treeScores[desc] += tree
					forestScores[desc] += forest
		f.close()
		numFiles+=1
for desc in sorted(treeScores.keys(), key=lambda x: treeScores[x]):
	print desc+"\t"+str(treeScores[desc])+"\t"+str(forestScores[desc])
print '\n'+str(numFiles)