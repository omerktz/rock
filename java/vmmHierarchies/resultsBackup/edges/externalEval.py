import statistics
import os

def collectSuccessors(filename):
	f = open(filename,'r')
	lines =[]
	for line in f.readlines():
		if len(line.strip()) > 0:
			lines.append(line)
	lines = lines[1:]
	children = {}
	for i in range(0,len(lines)):
		childs = set()
		prefix = lines[i][0:lines[i].find(lines[i].strip()[0])]
		j = i+1
		while j < len(lines):
			jPrefix = lines[j][0:lines[j].find(lines[j].strip()[0])]
			if (len(jPrefix) > len(prefix)):
				childs.add(lines[j].strip())
			else:
				break
			j+=1
		children[lines[i].strip()] = childs
	f.close()
	return children

def evalBenchmark(treeFile,groundtruthFile):
	constructed = collectSuccessors(treeFile)
	groundtruth = collectSuccessors(groundtruthFile)
	added = []
	missing = []
	for t in groundtruth.keys():
		missing.append(len(filter(lambda x: x not in constructed[t], groundtruth[t])))
		added.append(len(filter(lambda x: x not in groundtruth[t], constructed[t])))
	if len(missing) == 0:
		missing = [0,0]
	if len(added) == 0:
		added = [0,0]
	#print benchmark+"\tMissing successors:\tNum types="+str(len(groundtruth.keys()))+"\tAverage="+str(statistics.mean(missing))+"\tVariance="+str(statistics.variance(missing))+"\tStandard Deviation="+str(statistics.stdev(missing))
	#print benchmark+"\tAdded successors:\tNum types="+str(len(groundtruth.keys()))+"\tAverage="+str(statistics.mean(added))+"\tVariance="+str(statistics.variance(added))+"\tStandard Deviation="+str(statistics.stdev(added))
	return (sum(missing),sum(added))

configTreeMissing = {}
configEdgesMissing = {}
configForestMissing = {}
configTreeAdded = {}
configEdgesAdded = {}
configForestAdded = {}
for d in os.listdir('.'):
	if os.path.isdir(d):
		newPath = os.path.join('.',d)
		for f in os.listdir(newPath):
			if f.endswith('.groundtruth'):
				benchmark = f[:-len('.groundtruth')]
		groundtruth = os.path.join(newPath,benchmark+'.groundtruth')
		for dd in os.listdir(newPath):
			newnewPath = os.path.join(newPath,dd)
			if os.path.isdir(newnewPath):
				for f in os.listdir(newnewPath):
					if f.endswith('.tree'):
						f = f[:-len('tree')]
						config = f[len(benchmark)+1:-1]
						treePath = os.path.join(newnewPath,f+'tree')
						edgePath = os.path.join(newnewPath,f+'edges')
						forestPath = os.path.join(newnewPath,f+'forest')
						treeRes = evalBenchmark(treePath,groundtruth)
						edgesRes = evalBenchmark(edgePath,groundtruth)
						forestRes = evalBenchmark(forestPath,groundtruth)
						if config not in configTreeMissing.keys():
							configTreeMissing[config] = 0
							configEdgesMissing[config] = 0
							configForestMissing[config] = 0
							configTreeAdded[config] = 0
							configEdgesAdded[config] = 0
							configForestAdded[config] = 0
						configTreeMissing[config] += treeRes[0]
						configEdgesMissing[config] += edgesRes[0]
						configForestMissing[config] += forestRes[0]
						configTreeAdded[config] += treeRes[1]
						configEdgesAdded[config] += edgesRes[1]
						configForestAdded[config] += forestRes[1]
for c in sorted(configForestAdded.keys(), key=lambda x: configEdgesMissing[x]):
	print c+'\t'+str(configTreeMissing[c])+'\t'+str(configEdgesMissing[c])+'\t'+str(configForestMissing[c])+'\t'+str(configTreeAdded[c])+'\t'+str(configEdgesAdded[c])+'\t'+str(configForestAdded[c])

