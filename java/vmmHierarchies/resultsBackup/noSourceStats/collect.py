edgeScores = {}
treeScores = {}
forestScores = {}
import os
numFiles = 0
for file in os.listdir('.'):
	if file.endswith('.stats'):
		f = open(file,'r')
		first= True
		for line in f.readlines():
			parts = line.split('\t')
			if first:
				first = False
				baselineForest = float(parts[1][parts[1].find('=')+1:])
				baselineTree = float(parts[2][parts[2].find('=')+1:])
				baselineEdge = float(parts[3][parts[3].find('=')+1:])
			else:
				edge = float(parts[-4]) - baselineEdge
				tree = float(parts[-5]) - baselineTree
				forest = float(parts[-6]) - baselineForest
				desc = '\\t'.join(parts[1:-6])
				if desc not in treeScores.keys():
					edgeScores[desc] = edge
					treeScores[desc] = tree
					forestScores[desc] = forest
				else:
					edgeScores[desc] += edge
					treeScores[desc] += tree
					forestScores[desc] += forest
		f.close()
		numFiles+=1
for desc in sorted(treeScores.keys(), key=lambda x: treeScores[x]):
	print desc+"\t"+str(edgeScores[desc])+"\t"+str(treeScores[desc])+"\t"+str(forestScores[desc])
print '\n'+str(numFiles)