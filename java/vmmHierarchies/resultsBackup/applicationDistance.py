import statistics
import os
import sys

def collectSuccessorsTree(filename):
	f = open(filename,'r')
	lines =[]
	for line in f.readlines():
		if len(line.strip()) > 0:
			lines.append(line)
	lines = lines[1:]
	children = {}
	for i in range(0,len(lines)):
		childs = set()
		prefix = lines[i][0:lines[i].find(lines[i].strip()[0])]
		j = i+1
		while j < len(lines):
			jPrefix = lines[j][0:lines[j].find(lines[j].strip()[0])]
			if (len(jPrefix) > len(prefix)):
				childs.add(lines[j].strip())
			else:
				break
			j+=1
		children[lines[i].strip()] = childs
	f.close()
	return children

def collectSuccessorsList(filename):
	f = open(filename,'r')
	lines =[]
	directChildren ={}
	child = ''
	types = set()
	for line in f.readlines():
		if len(line.strip()) == 0:
			continue
		else:
			if line.strip() == '====================================================':
				continue
			else:
				if not line.startswith('\t'):
					child = line[:line.rfind('[')].strip()
					types.add(child)
				else:
					line = line.strip()[:-1].strip()
					parent = line[:line.rfind('\t')]
					if parent not in directChildren.keys():
						directChildren[parent] = set()
					directChildren[parent].add(child)
	f.close()
	children = {}
	for i in types:
		childs = set()
		if i in directChildren.keys():
			newChilds = set()
			newChilds.update(directChildren[i])
			while len(newChilds) > 0:
				c = newChilds.pop()
				childs.add(c)
				if c in directChildren.keys():
					newChilds.update(directChildren[c])
					newChilds.difference_update(childs)
					newChilds.discard(i)
		children[i] = childs
	return children

def evalBenchmark(benchmark,filename,output,dir):
	strippedBenchmark = benchmark[:l.rfind('.')]
	constructed = collectSuccessorsTree(os.path.join('.','edges',strippedBenchmark,dir,benchmark+'.hierarchy'+filename+'.tree'))
	structural = collectSuccessorsList(os.path.join('.','edges',strippedBenchmark,'Dummy',benchmark+'.hierarchy'+'.Qfalse.Rfalse.Dummy.1'+'.closest'))
	groundtruth = collectSuccessorsTree(os.path.join('.','edges',strippedBenchmark,benchmark+'.groundtruth'))
	addedModel = []
	missingModel = []
	addedStruct = []
	missingStruct = []
	for t in groundtruth.keys():
		missingModel.append(len(filter(lambda x: x not in constructed[t], groundtruth[t])))
		addedModel.append(len(filter(lambda x: x not in groundtruth[t], constructed[t])))
		missingStruct.append(len(filter(lambda x: x not in structural[t], groundtruth[t])))
		addedStruct.append(len(filter(lambda x: x not in groundtruth[t], structural[t])))
	if len(missingModel) == 0:
		missingModel = [0,0]
	if len(addedModel) == 0:
		addedModel = [0,0]
	if len(missingStruct) == 0:
		missingStruct = [0,0]
	if len(addedStruct) == 0:
		addedStruct = [0,0]
	output.write(strippedBenchmark+' & '+str(len(groundtruth.keys()))+' & '+str(round(statistics.mean(missingStruct),2))+' & '+str(round(statistics.mean(addedStruct),2))+' & '+str(round(statistics.mean(missingModel),2))+' & '+str(round(statistics.mean(addedModel),2))+' \\\\\n')

if len(sys.argv) < 7:
	print 'Usage: python '+sys.argv[0]+' <Q:true/false> <R:true/false> <DKL/JSD/JSDsqrt> <M/B/A> <1/10> <output>'
	sys.exit(1)
filename = '.Q'+sys.argv[1]+'.R'+sys.argv[2]+'.'+sys.argv[3]+'_'+sys.argv[4]+'.'+sys.argv[5]
with open(sys.argv[6],'w') as output:
	with open('benchmarks.list','r') as benchmarks:
		for l in benchmarks.readlines():
			l = l.strip()
			if len(l) > 0:
				if os.path.exists(os.path.join('.','edges',l[:l.rfind('.')])):
					evalBenchmark(l,filename,output,sys.argv[3])
				else:
					output.write(l+'\n')
