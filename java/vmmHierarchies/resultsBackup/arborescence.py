import statistics
import os
import sys

from  edmonds import getArborescence

def getChildren(t,h):
	children = set()
	for c in h[t]:
		children.add(c)
		children.update(getChildren(c,h))
	return children

def collectSuccessorsTree(filename):
	f = open(filename,'r')
	g = {}
	child = ''
	types = set()
	for line in f.readlines():
		if len(line.strip()) == 0:
			continue
		else:
			if line.strip() == '====================================================':
				continue
			else:
				if not line.startswith('\t'):
					child = line[:line.rfind('[')].strip()
					types.add(child)
				else:
					line = line.strip()
					if line[-1] not in map(lambda d:str(d),range(10)):
						line = line[:-1].strip()
					parent = line[:line.rfind('\t')]
					if child not in g.keys():
						g[child] = {}
					g[child][parent] = float(line[line.rfind('\t'):].strip())
	f.close()
	for a in getArborescence(g):
		hierarchy = {}
		for t in types:
			hierarchy[t] = set()
		for n in a:
			hierarchy[a[n]].add(n)
		children = {}
		for t in hierarchy.keys():
			children[t] = getChildren(t,hierarchy)
		yield children

def collectGroundtruthTree(filename):
	f = open(filename,'r')
	lines =[]
	for line in f.readlines():
		if len(line.strip()) > 0:
			lines.append(line)
	lines = lines[1:]
	children = {}
	for i in range(0,len(lines)):
		childs = set()
		prefix = lines[i][0:lines[i].find(lines[i].strip()[0])]
		j = i+1
		while j < len(lines):
			jPrefix = lines[j][0:lines[j].find(lines[j].strip()[0])]
			if (len(jPrefix) > len(prefix)):
				childs.add(lines[j].strip())
			else:
				break
			j+=1
		children[lines[i].strip()] = childs
	f.close()
	return children

def collectSuccessorsList(filename):
	f = open(filename,'r')
	directChildren = {}
	child = ''
	types = set()
	for line in f.readlines():
		if len(line.strip()) == 0:
			continue
		else:
			if line.strip() == '====================================================':
				continue
			else:
				if not line.startswith('\t'):
					child = line[:line.rfind('[')].strip()
					types.add(child)
				else:
					line = line.strip()[:-1].strip()
					parent = line[:line.rfind('\t')]
					if parent not in directChildren.keys():
						directChildren[parent] = set()
					directChildren[parent].add(child)
	f.close()
	children = {}
	for i in types:
		childs = set()
		if i in directChildren.keys():
			newChilds = set()
			newChilds.update(directChildren[i])
			while len(newChilds) > 0:
				c = newChilds.pop()
				childs.add(c)
				if c in directChildren.keys():
					newChilds.update(directChildren[c])
					newChilds.difference_update(childs)
					newChilds.discard(i)
		children[i] = childs
	return children

def evalBenchmark(benchmark,filename,output,dir):
	strippedBenchmark = benchmark[:benchmark.rfind('.')]
	structural = collectSuccessorsList(os.path.join('.','edges',strippedBenchmark,dir,benchmark+'.hierarchy'+filename+'.closest'))
	groundtruth = collectGroundtruthTree(os.path.join('.','edges',strippedBenchmark,benchmark+'.groundtruth'))
	addedStruct = []
	missingStruct = []
	for t in groundtruth.keys():
		missingStruct.append(len(filter(lambda x: x not in structural[t], groundtruth[t])))
		addedStruct.append(len(filter(lambda x: x not in groundtruth[t], structural[t])))
	if len(missingStruct) == 0:
		missingStruct = [0,0]
	if len(addedStruct) == 0:
		addedStruct = [0,0]
	output.write(strippedBenchmark+' & '+str(len(groundtruth.keys()))+' & '+str(round(statistics.mean(missingStruct),2))+' & '+str(round(statistics.mean(addedStruct),2)))
	for constructed in collectSuccessorsTree(os.path.join('.','edges',strippedBenchmark,dir,benchmark+'.hierarchy'+filename+'.closest')):
		addedModel = []
		missingModel = []
		for t in groundtruth.keys():
			missingModel.append(len(filter(lambda x: x not in constructed[t], groundtruth[t])))
			addedModel.append(len(filter(lambda x: x not in groundtruth[t], constructed[t])))
		if len(missingModel) == 0:
			missingModel = [0,0]
		if len(addedModel) == 0:
			addedModel = [0,0]
		output.write(' & '+str(round(statistics.mean(missingModel),2))+' & '+str(round(statistics.mean(addedModel),2)))
	output.write(' \\\\\n')

#if len(sys.argv) < 7:
#	print 'Usage: python '+sys.argv[0]+' <Q:true/false> <R:true/false> <DKL/JSD/JSDsqrt> <M/B/A> <1/10> <output>'
#	sys.exit(1)
if len(sys.argv) < 2:
	print 'Usage: python '+sys.argv[0]+' <output>'
	sys.exit(1)
def runTest(a,b,c,d):
	filename = '.Q'+a+'.R'+b+'.'+'DKL'+'_'+c+'.'+d
	outputName = sys.argv[1]+'_'+a+'_'+b+'_'+c+'_'+d
	#if (c,d)in [('M','10'),('B','10'),('A','1')]:
	#	continue
	try:
		with open(outputName,'w') as output:
			with open('benchmarks.list','r') as benchmarks:
				for l in benchmarks.readlines():
					l = l.strip()
					if len(l) > 0:
						if os.path.exists(os.path.join('.','edges',l[:l.rfind('.')])):
							evalBenchmark(l,filename,output,'DKL')
						else:
							output.write(l+'\n')
	except:
		os.remove(outputName)
		print 'failed '+str((a,b,c,d))
import itertools
map(lambda x:runTest(*x),itertools.product(['true','false'],['true','false'],['M','B','A'],['1','10']))
