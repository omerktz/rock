class hashableSet(set):
    def __hash__(self):
        return hash(str(sorted(self)))
set=hashableSet
class hashableDict(dict):
    def __hash__(self):
        return hash(tuple(sorted(self.items())))
dict=hashableDict

def getParent(g,n):
    if n in g.keys():
        return g[n][0]
    return None

def findCycle(g,r):
    n = getParent(g,r)
    nodes = set()
    while True:
        if not n:
            return None
        if n in nodes:
            return None
        nodes.add(n)
        if r == n:
            return nodes
        n = getParent(g,n)

def findAllCycles(g):
    return map(lambda x:list(x),filter(lambda c:c,set(map(lambda n:findCycle(g,n),g.keys()))))

def getCheapestParent(p):
    x = sorted(p.keys(),key=lambda x:p[x])[0]
    return map(lambda c:(c,p[c][1]),filter(lambda k:p[k][0]==p[x][0],p.keys()))

import itertools
def getCheapestParents(g,p):
    parents = map(lambda x:map(lambda y:(x,y),getCheapestParent(g[x])),p)
    for e in itertools.product(*parents):
        yield dict(e)

#based on https://en.wikipedia.org/wiki/Edmonds%27_algorithm#Algorithm
def getArborescenceInner(g):
    #roots = filter(lambda n:len(g[n])==0,g.keys())
    #a = dict(map(lambda x:(x,getCheapestParent(g[x])),filter(lambda n:len(g[n])>0,g.keys())))
    for a in getCheapestParents(g,filter(lambda x:len(g[x])>0,g.keys())):
        cycles = findAllCycles(a)
        if len(cycles) == 0:
            yield a
        else:
            newN = {}
            newG = {}
            for n in g.keys():
                s = n
                sourceCycle = False
                for c in cycles:
                    if  n in c:
                        s = str(c)
                        sourceCycle = True
                if s not in newG.keys():
                    newG[s] = {}
                for e in g[n].keys():
                    d = e
                    destCycle = False
                    for c in cycles:
                        if e in c:
                            d = str(c)
                            destCycle = True
                    if destCycle and sourceCycle:
                        continue
                    cost = g[n][e]
                    if sourceCycle:
                        cost = (cost[0]-g[n][a[n][0]][0],cost[1])
                    if (d not in newG[s].keys()) or (cost < newG[s][d]):
                        newG[s][d] = cost
            for newA in getArborescenceInner(newG):
                finalA = {}
                allCycles = map(lambda c:str(c),cycles)
                edges = {}
                for n in g.keys():
                    for e in g[n].keys():
                        edges[g[n][e][1]] = (n,e)
                for n in newA.keys():
                    if (n in allCycles) or (newA[n][0] in allCycles):
                        edge = edges[newA[n][1]]
                        finalA[edge[0]] = (edge[1],newA[n][1])
                    else:
                        finalA[n] = newA[n]
                for c in cycles:
                    for n in c:
                        if n not in finalA.keys():
                            finalA[n] = a[n]
                yield finalA

def updateGraph(g):
    newG = {}
    for n in g.keys():
        newG[n] = {}
        for e in g[n].keys():
            newG[n][e]=(g[n][e],(n,e))
    return newG

def calcArborescence(a,g):
    return sum(map(lambda n:g[n][a[n]],a.keys()))

def getArborescence(g):
    def getMaxEdges(c):
        maxedge = max(map(lambda n:g[n][a[n][0]],c))
        return filter(lambda n:g[n][a[n][0]]==maxedge,c)
    allArbs = set()
    for a in getArborescenceInner(updateGraph(g)):
        cycles = findAllCycles(a)
        maxEdges = map(lambda c:getMaxEdges(c),cycles)
        for c in itertools.product(*maxEdges):
            tmpA = {}
            for n in a.keys():
                tmpA[n] = a[n]
            for x in c:
                del tmpA[x]
            result = {}
            for n in tmpA.keys():
                result[n] = tmpA[n][0]
            allArbs.add(dict(result))
    allArbs = map(lambda a:(a,calcArborescence(a,g)),allArbs)
    minArb = min(map(lambda a:a[1],allArbs))
    return map(lambda y:y[0],filter(lambda x:x[1]==minArb,allArbs))

if __name__ == "__main__":
    g={}
    g[1]={2:1,4:3,5:3}
    g[2]={3:1}
    g[3]={1:1}
    g[4]={7:2,10:2}
    g[8]={9:1}
    g[9]={8:1}
    print getArborescence(g)
