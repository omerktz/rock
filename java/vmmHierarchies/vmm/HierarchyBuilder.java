package vmm;

import com.mongodb.DBObject;

import apted.distance.APTED;
import apted.util.LblTree;
import vmm.DBWrapper.applyToResults;
import vmm.Forest.Node;
import vmm.DKLScorer.SeqsEnum;
import vmm.PairScore.Pair;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class HierarchyBuilder {
	
	private static final boolean DEBUG = false;
	private static final boolean SERIAL = false;

	@SuppressWarnings("rawtypes")
	protected static class ComparableStringArray implements Comparable {
		public String[] data;
		
		public ComparableStringArray(String[] data) {
			this.data = data;
		}

		@Override
		public int compareTo(Object o) {
			if (o.getClass().equals(this.getClass())) {
				ComparableStringArray other = (ComparableStringArray)o;
				int limit = Math.min(data.length, other.data.length);
				for (int i = 0; i < limit; i++) {
					int res = data[i].compareTo(other.data[i]);
					if (res != 0) {
						return res;
					}
				}
				return new Integer(data.length).compareTo(other.data.length);
			}
			return 1;
		}
		
		public boolean equals(ComparableStringArray other) {
		   if (data.length != other.data.length) {
			   return false;
		   }
		   return (this.compareTo(other) == 0);
		}
	}
	
	private final String printPrefix;
	
	private static final String outputDir = "hierarchyResults/";

	private static final int maxK = 7;
	private static final int minK = 1;

	private static final String addressDir = "addresses/";
	private final Map<String,String> names;
	private final Map<String,String> rnames;

	private final DBWrapper db;

	private final String binary;
	private final String strippedBinary;

	private final int numActions;
	private final int maxLength;
	
	private final boolean refSubs;
	private final boolean querySubs;
	
	private static final String clustersDir = "clusters/";
	private final List<Set<String>> clusters;
	
	protected final Map<String,Set<ComparableStringArray>> path2code;

	protected final Map<String,Set<String>> typePaths;
	protected final Map<String,Set<String>> typeAllPaths;
	
	protected final ModelUtils modelUtils;

	private final PairScore scorer;
	
	private final String filenameAddition;
	
	private static final String sourceDir = "source/";
	public Set<String> allTypes;
	//private final Set<String> parentless;
	private final Map<String,String> parents;
	private final Map<String,Set<String>> children;
	private final Map<String,Set<String>> siblings;
	
	private static final String rttiDir = "rtti/";
	
	private static final String analysisDir = "analysis/";
	private final Map<String,Set<String>> possibleParents;
	private class AnalysisType {
		public int vtsize;
		public int asize;
		public Set<String> parents;
		public Set<Integer> virtuals;
		
		public AnalysisType(int vtsize, int asize, Set<String> parents, Set<Integer> virtuals) {
			this.vtsize = vtsize;
			this.asize = asize;
			this.parents = parents;
			this.virtuals = virtuals;
		}
	}
	private final Map<String, AnalysisType> analysisTypes;
	
	public final Map<String,Map<Pair<Set<String>>,Double>> closestTypes;
	
	private final Forest groundTruthForest;
	
	private final APTED ted;
	
	public HierarchyBuilder(String binary, boolean querySubs, boolean refSubs, PairScore scorer, int numRepeatLearn, String printPrefix) {
		this.printPrefix = printPrefix;
		this.binary = binary;
		this.strippedBinary = binary.substring(0,binary.lastIndexOf('.'));
		this.names = new TreeMap<String,String>();
		this.rnames = new TreeMap<String,String>();
		loadNames();
		this.db = new DBWrapper(binary);
		this.numActions = countCodes();
		this.maxLength = getMaxK();	
		this.refSubs = refSubs;
		this.querySubs = querySubs;
		this.path2code = new HashMap<String,Set<ComparableStringArray>>();
		loadPath2Code();
		this.typePaths = new HashMap<String,Set<String>>();
		this.typeAllPaths = new HashMap<String,Set<String>>();
		loadTypeSignatures();
		this.allTypes = new TreeSet<String>();
		//this.parentless = new TreeSet<String>();
		this.parents = new HashMap<String,String>();
		this.children = new HashMap<String,Set<String>>();
		this.siblings = new HashMap<String,Set<String>>();
		loadSource();
		loadRtti();
		computeChildren();
		cleanParentsNoise();
		this.groundTruthForest = new Forest();
		computeGroundTruthForest();
		this.clusters = new LinkedList<Set<String>>();
		loadClusters();
		this.possibleParents = new HashMap<String,Set<String>>();
		computePossibleParents();
		this.analysisTypes = new HashMap<String, AnalysisType>();
		loadAnalysisData();
		updatePossibleParents();
		this.modelUtils = new ModelUtils(numActions, maxLength, this, numRepeatLearn);
		this.scorer = scorer;
		this.filenameAddition = ".Q"+querySubs+".R"+refSubs+scorer.getFilenameAddition()+"."+numRepeatLearn;
		this.closestTypes = new HashMap<String,Map<Pair<Set<String>>,Double>>();
		this.ted= new APTED((float)1.0, (float)1.0, (float)1.0);
	}
	public HierarchyBuilder(String binary, boolean querySubs, boolean refSubs, PairScore scorer, String printPrefix) {
		this(binary, refSubs, querySubs, scorer, 1, printPrefix);
	}

	private void loadNames() {
		if (DEBUG) System.out.print(printPrefix+"Loading names...");
		BufferedReader input;
		Map<String,String> tmp1 = new HashMap<String,String>();
		Map<String,String> tmp2 = new HashMap<String,String>();
		try {
			input = new BufferedReader(new FileReader(addressDir+binary+".addresses.unstripped"));
			String line;
			while ((line = input.readLine()) != null) {
				String[] parts = line.split("\t");
//				if (parts[0].trim().endsWith("::`local vftable'")) {
//					parts[0] = parts[0].trim();
//					parts[0] = parts[0].trim().substring(0, parts[0].length()-("::`local vftable'".length()));
//					System.out.println(parts[0]);
//				}
				tmp1.put(parts[1],parts[0]);
			}
			input.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			input = new BufferedReader(new FileReader(addressDir+binary+".addresses.stripped"));
			String line;
			while ((line = input.readLine()) != null) {
				String[] parts = line.split("\t");
				tmp2.put(parts[0],parts[1]);
			}
			input.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		for (String s : tmp2.keySet()) {
			if (tmp1.keySet().contains(tmp2.get(s))) {
				names.put(s, tmp1.get(tmp2.get(s)));
				rnames.put(tmp1.get(tmp2.get(s)),s);
			} else {
				names.put(s, s);
				rnames.put(s, s);
			}
		}
		if (DEBUG) System.out.println("\tDone!");
	}
	
	private Map<String,Object> getNewQuery() {
		Map<String,Object> query = new HashMap<String,Object>();
		query.put("binary",this.binary);	
		return query;
	}
	
	private int countCodes() {
		Map<String,Object> query = getNewQuery();
		query.put("k", 1);
		return db.distinctInCollection("typesignatures", "code", query).size();
	}
	
	private int getMaxK() {
		Map<String,Object> query = getNewQuery();
		Set<Integer> ks = db.distinctIntegersInCollection("typesignatures", "k", query);
		if (ks.size() == 0) {
			return 0;
		}
		return Collections.max(ks);
	}
	
	private String[] splitTracelet(String code) {
		String[] strCodes = code.split(";");
		if (strCodes[strCodes.length-1].trim().isEmpty()) {
			String[] tmp = new String[strCodes.length-1];
			for (int i = 0; i < tmp.length; i++) {
				tmp[i] = strCodes[i];
			}
			strCodes = tmp;
		}
		return strCodes;
	}
	
	private void loadPath2Code() {
		if (DEBUG) System.out.print(printPrefix+"Loading paths...");
		Map<String,Object> query = getNewQuery();
		db.findInCollectionAndApply("typed", query, new applyToResults() {
			@Override
			public void apply(DBObject obj) {
				String path = (String) obj.get("trace");
				if (!path2code.containsKey(path)) {
					path2code.put(path, new TreeSet<ComparableStringArray>());
				}
				String code = (String) obj.get("code");
				String[] strCodes = splitTracelet(code);
				path2code.get(path).add(new ComparableStringArray(strCodes));
			}
		});
		if (DEBUG) System.out.println("\tDone!");
	}

	private Set<String> getAllSubpathsOfLen(String[] paths, int len) {
		Set<String> res = new TreeSet<String>();
		for (int i=0; i < (paths.length-len); ++i) {
			StringBuilder s = new StringBuilder();
			for (int j=0; j<len; ++j) {
				s.append(paths[i+j]);
				s.append(',');
			}
			res.add(s.toString());
		}
		return res;
	}
	private Set<String> getAllSubpaths(String path) {
		Set<String> subpaths = new TreeSet<String>();
		if (path.endsWith(",")) {
			path = path.substring(0, path.length()-1);
		}
		String[] paths = path.split(",");
		for (int i=1; i < paths.length; ++i) {
			subpaths.addAll(getAllSubpathsOfLen(paths, i));
		}
		return subpaths;
	}
	
	private void loadTypeSignatures() {
		if (DEBUG) System.out.print(printPrefix+"Loading type data...");
		Map<String,Object> query = getNewQuery();
		for (final String type: db.distinctInCollection("typesignatures", "typename", query)) {
			Map<String,Object> innerQuery = getNewQuery();
			innerQuery.put("typename", type);
			final Set<String> data = new TreeSet<String>();
			final Set<String> allData = new TreeSet<String>();
			final Set<String> paths = new TreeSet<String>();
			for (int k = maxK; k >= minK; k--) {
				innerQuery.put("k", k);
				db.findInCollectionAndApply("typed", innerQuery, new applyToResults() {
					@Override
					public void apply(DBObject obj) {
						String path = (String) obj.get("trace");
						allData.add(path);
						if (!paths.contains(path)) {
							data.add(path);
						}
						paths.add(path);
						paths.addAll(getAllSubpaths(path));
					}
				});
				innerQuery.remove("k");
			}
			typePaths.put(type, data);
			typeAllPaths.put(type, allData);
		}
		if (DEBUG) System.out.println("\tDone!");
	}
	
	private void loadSource() {
		if (DEBUG) System.out.print(printPrefix+"Loading ground truth...");
		BufferedReader input;
		try {
			input = new BufferedReader(new FileReader(sourceDir+strippedBinary+".source"));
			String line;
			int stage = 1;
			while ((line = input.readLine()) != null) {
				line = line.trim();
				if (stage == 3) {
					if (line.isEmpty()) {
						++stage;
					} else {
						// Do nothing
					}
				}
				if (stage == 2) {
					if (line.isEmpty()) {
						++stage;
					} else {
						if (rnames.containsKey(line)) {
							line = rnames.get(line);
						}
						//parentless.add(line);
						allTypes.add(line);
					}				
				}
				if (stage == 1) {
					if (line.isEmpty()) {
						++stage;
					} else {
						String[] parts = line.split(" :\t");
						assert parts.length == 2;
						String type = parts[0].trim();
						if (rnames.containsKey(type)) {
							type = rnames.get(type);
						}
						String parent = parts[1].trim();
						parent = parent.substring(2, parent.length()-2);
						if (rnames.containsKey(parent)) {
							parent = rnames.get(parent);
						}
						parents.put(type, parent);
						allTypes.add(type);
						allTypes.add(parent);
					}
				}
			}
			input.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (DEBUG) System.out.println("\tDone!");
	}
	
	private void loadRtti() {
		if (DEBUG) System.out.print(printPrefix+"Loading RTTI ground truth...");
		BufferedReader input;
		try {
			input = new BufferedReader(new FileReader(rttiDir+binary+".rtti"));
			String line = input.readLine();
			assert line.trim().equals("All:");
			while (!input.readLine().trim().isEmpty()) ;
			line = input.readLine();
			assert line.trim().equals("Reduced:");
			while ((line = input.readLine()) != null) {
				if (line.contains(" : ")) {
					String[] parts = line.split(" : ");
					String type = parts[0].trim();
					String ps = parts[1].trim();
					String[] psParts = ps.substring(1, ps.length()-1).split(", ");
					String parent = psParts[0].trim();
					parent = parent.substring(1, parent.length()-1);
					if (rnames.containsKey(type)) {
						type = rnames.get(type);
					}
					if (rnames.containsKey(parent)) {
						parent = rnames.get(parent);
					}
					parents.put(type, parent);
					allTypes.add(type);
					allTypes.add(parent);
				}
			}
			input.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (DEBUG) System.out.println("\tDone!");
	}
	
	private void computeChildren() {
		if (DEBUG) System.out.print(printPrefix+"Computing children...");
		for (String type : parents.keySet()) {
			String parent = parents.get(type);
			if (!children.containsKey(parent)) {
				children.put(parent, new TreeSet<String>());
			}
			children.get(parent).add(type);
		}
		for (String type : parents.keySet()) {
			Set<String> sibling = new TreeSet<String>();
			sibling.addAll(children.get(parents.get(type)));
			sibling.remove(type);
			siblings.put(type, sibling);
		}
		if (DEBUG) System.out.println("\tDone!");
	}
	
	private void cleanParentsNoise() {
		allTypes.retainAll(this.typePaths.keySet());
		Set<String> toRemove = new TreeSet<String>();
		for (String type: children.keySet()) {
			children.get(type).retainAll(allTypes);
			if (!allTypes.contains(type)) {
				toRemove.add(type);
				String parent = type;
				while (!allTypes.contains(parent))
				{
					if (parents.containsKey(parent)) {
						parent = parents.get(parent);
					} else {
						parent = null;
						break;
					}
				}
				if (parent != null) {
					children.get(parent).addAll(children.get(type));
				}
			}
		}
		for (String t : toRemove) {
			children.remove(t);
		}
		toRemove.clear();
		for (String type : parents.keySet()) {
			if (!allTypes.contains(type)) {
				toRemove.add(type);
			} else {
				while (!allTypes.contains(parents.get(type))) {
					if (parents.containsKey(parents.get(type))) {
						parents.put(type, parents.get(parents.get(type)));
					} else {
						toRemove.add(type);
						break;
					}
				}
			}
		}
		for (String t : toRemove) {
			parents.remove(t);
		}
		toRemove.clear();
		for (String type: siblings.keySet()) {
			if (allTypes.contains(type)) {
				siblings.get(type).retainAll(allTypes);
			} else {
				toRemove.add(type);
			}
		}
		for (String t : toRemove) {
			siblings.remove(t);
		}
	}
	
	private void computeGroundTruthForest(){
		if (DEBUG) System.out.print(printPrefix+"Computing ground truth forest...");
		Set<String> nodes = new TreeSet<String>();
		for (String type : parents.keySet()) {
			String parent = parents.get(type);
			nodes.add(type);
			nodes.add(parent);
			if (names.containsKey(type)) {
				type = names.get(type);
			}
			if (names.containsKey(parent)) {
				parent = names.get(parent);
			}
			groundTruthForest.addToForest(type, parent);
		}
		for (String type : allTypes) {
			if (!nodes.contains(type)) {
				if (names.containsKey(type)) {
					type = names.get(type);
				}
				groundTruthForest.addToForest(type);
			}
		}
		if (DEBUG) System.out.println("\tDone!");
	}
	
	private void loadClusters() {
		if (DEBUG) System.out.print(printPrefix+"Loading clusters...");
		Set<String> types = new TreeSet<String>();
		final BufferedReader reader;
		try {
			reader = new BufferedReader(new FileReader(clustersDir+binary+".clusters"));
			String line;
			while ((line = reader.readLine()) != null) {
				line = line.trim();
				if (line.length() > 0) {
					Set<String> cluster = new TreeSet<String>();
					for (String x : line.split(", ")) {
						String name = rnames.get(x);
						if (allTypes.contains(name)) {
							types.add(name);
							cluster.add(name);
						}
					}
					this.clusters.add(cluster);
				}
			}
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		for (String type : allTypes) {
			if (!types.contains(type)) {
				Set<String> cluster = new TreeSet<String>();
				cluster.add(type);
				this.clusters.add(cluster);
			}
		}
		if (DEBUG) System.out.println("\tDone!");
	}
	
	private void computePossibleParents() {
		if (DEBUG) System.out.print(printPrefix+"Computing possible parents...");
		for (Set<String> cluster : clusters) {
			for (String c : cluster) {
				Set<String> parents = new TreeSet<String>();
				parents.addAll(cluster);
				parents.remove(c);
				possibleParents.put(c, parents);
			}
		}
		if (DEBUG) System.out.println("\tDone!");
	}
	
	private void loadAnalysisData() {
		if (DEBUG) System.out.print(printPrefix+"Loading analysis data...");
		final BufferedReader reader;
		try {
			reader = new BufferedReader(new FileReader(analysisDir+binary+".analysis"));
			String line;
			int phase = 1;
			String type = null;
			int vtsize = -1;
			int asize = -1;
			Set<String> parents = new TreeSet<String>();
			Set<Integer> virtuals = new TreeSet<Integer>(); 
			while ((line = reader.readLine()) != null) {
				if (line.trim().isEmpty()) {
					analysisTypes.put(type, new AnalysisType(vtsize,asize,parents,virtuals));
					phase = 1;
					type = null;
					vtsize = -1;
					asize = -1;
					parents = new TreeSet<String>();
					virtuals = new TreeSet<Integer>(); 
				} else {
					if (phase == 6) {
						line = line.trim();
						line = line.substring(line.indexOf(' ')+2, line.length()-1);
						for (String i : line.split(", ")) {
							virtuals.add(Integer.parseInt(i));
						}
						++phase;						
					}
					else if (phase == 5) {
						if (line.startsWith("\t\t")) {
							while ((line = reader.readLine()).startsWith("\t\t"));
						}
						assert line.trim().equals("Virtual:");
						++phase;
					}
					else if (phase == 4) {
						assert line.trim().equals("Parents:");
						line = reader.readLine();
						if (line.startsWith("\t\t")) {
							line = line.trim();
							line = line.substring(1, line.length()-1);
							for (String p : line.split(", ")) {
								p = p.substring(1, p.length()-1);
								if (rnames.containsKey(p)) {
									p = rnames.get(p);
								}
								parents.add(p);
							}
							line = reader.readLine();
						}
						assert line.trim().equals("Related:");
						++phase;
					}
					else if (phase == 3) {
						assert line.startsWith("\tAllocated size:\t");
						String[] parts = line.trim().split("\t");
						assert parts.length == 3;
						asize = Integer.parseInt(parts[1]);
						++phase;
					}
					else if (phase == 2) {
						assert line.startsWith("\tVTable size:\t");
						String[] parts = line.trim().split("\t");
						assert parts.length == 2;
						vtsize = Integer.parseInt(parts[1]);
						++phase;
					}
					else if (phase == 1) {
						type = line;
						if (rnames.containsKey(type)) {
							type= rnames.get(type);
						}
						++phase;
					}
				}
			}
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (DEBUG) System.out.println("\tDone!");
	}
	
	private void updatePossibleParents() {
		if (DEBUG) System.out.print(printPrefix+"Updating possible parents...");
		for (String t : analysisTypes.keySet()) {
			if (possibleParents.containsKey(t)) {
				AnalysisType ac = analysisTypes.get(t);
				Set<String> possible = possibleParents.get(t);
				if (ac.parents.size() > 0) {
					possible.retainAll(ac.parents);
				}
				Set<String> remove = new TreeSet<String>();
				for (String p : possible) {
					if (analysisTypes.containsKey(p)) {
						AnalysisType pac = analysisTypes.get(p);
						/* removed because asize is not reliable
						if (pac.asize > ac.asize) {
							remove.add(p);
						}*/
						if (pac.vtsize > ac.vtsize) {
							remove.add(p);
						}
						if (!pac.virtuals.containsAll(ac.virtuals)) {
							remove.add(p);
						}
						if (pac.parents.contains(t)) {
							remove.add(p);
						}
					}
				}
				possible.removeAll(remove);
				//assert possible.size() > 0; // disabled because no possible parents might help re-clustering
			}
		}
		if (DEBUG) System.out.println("\tDone!");
	}
	
	protected Set<String> getAllPaths(Set<String> types) {
		if (types.size() == 1) {
			return typeAllPaths.get(types.toArray()[0]);
		}
		Set<String> paths = new TreeSet<String>();
		for (String type: types) {
			paths.addAll(typeAllPaths.get(type));
		}
		return paths;
	}
	
	protected Set<String> getPaths(Set<String> types) {
		if (types.size() == 1) {
			return typePaths.get(types.toArray()[0]);
		}
		Set<String> paths = new TreeSet<String>();
		Set<String> subpaths = new TreeSet<String>();
		for (String type: types) {
			for (String path : typePaths.get(type)) {
				subpaths.addAll(getAllSubpaths(path));
			}
		}
		for (String type: types) {
			for (String path : typePaths.get(type)) {
				if (!subpaths.contains(path)) {
					paths.add(path);
				}
			}
		}
		return paths;
	}
	
	protected Collection<ComparableStringArray> getAllCodes(Set<String> types) {
		Set<String> paths = getAllPaths(types);
		Collection<ComparableStringArray> codes = new LinkedList<ComparableStringArray>();
		for (String path: paths) {
			codes.addAll(path2code.get(path));
		}
		return codes;
	}
	
	protected Collection<ComparableStringArray> getCodes(Set<String> types) {
		Set<String> paths = getPaths(types);
		Collection<ComparableStringArray> codes = new LinkedList<ComparableStringArray>();
		for (String path: paths) {
			codes.addAll(path2code.get(path));
		}
		return codes;
	}

	protected Set<String> getRefPaths(Set<String> types) {
		if (refSubs) return getAllPaths(types);
		else return getPaths(types);
	}
	
	protected Set<String> getQueryPaths(Set<String> types) {
		if (querySubs) return getAllPaths(types);
		else return getPaths(types);
	}
	
	Map<Set<String>,Collection<ComparableStringArray>> refCodesCache = new HashMap<Set<String>, Collection<ComparableStringArray>>();
	protected Collection<ComparableStringArray> getRefCodes(Set<String> types) {
		if (!refCodesCache.containsKey(types)) {
			if (refSubs) refCodesCache.put(types, getAllCodes(types));
			else refCodesCache.put(types, getCodes(types));
		}
		return refCodesCache.get(types);
	}
	
	Map<Set<String>,Collection<ComparableStringArray>> queryCodesCache = new HashMap<Set<String>, Collection<ComparableStringArray>>();
	protected Collection<ComparableStringArray> getQueryCodes(Set<String> types) {
		if (!queryCodesCache.containsKey(types)) {
			if (querySubs) queryCodesCache.put(types, getAllCodes(types));
			else queryCodesCache.put(types, getCodes(types));
		}
		return queryCodesCache.get(types);
	}
	
	private Set<String> wrapInSet(String data) {
		Set<String> res = new TreeSet<String>();
		res.add(data);
		return res;
	}
	
	private double getVariance(Map<Pair<Set<String>>,Double> pairs) {
		List<Double> data = new LinkedList<Double>();
		for (Pair<Set<String>> p : pairs.keySet()) {
			data.add(scorer.getNormalizedScore(pairs, p));
		}
		double mean = 0.0;
        for(double a : data)
            mean += a;
        mean /= (double)data.size();
        double var = 0;
        for(double a :data)
        	var += (mean-a)*(mean-a);
        return var/data.size();
	}
	
	private Pair<Set<String>> createPair(String query, String reference) {
		Pair<Set<String>> p = new Pair<Set<String>>();
		p.query = new TreeSet<String>();
		p.query.add(query);
		p.reference = new TreeSet<String>();
		p.reference.add(reference);
		return p;
	}
	
	private static final String seperator = "====================================================";
	public void findClosest() {
		if (DEBUG) System.out.print(printPrefix+"Finding closest types...");
		closestTypes.clear();
//		File f = new File(outputDir+strippedBinary+'/'+scorer.getName()+'/'+binary+".hierarchy"+filenameAddition+".closest");
//		if (f.exists()) {
//			try {
//				BufferedReader input = new BufferedReader(new FileReader(f));
//				String line;
//				String type = null;
//				while ((line = input.readLine()) != null) {
//					if (line.trim().isEmpty()) {
//						continue;
//					} else if (line.trim().equals(seperator)) {
//						continue;
//					} else if (!line.startsWith("\t")) {
//						type = line.substring(0, line.lastIndexOf('['));
//						type = rnames.containsKey(type) ? rnames.get(type) : type;
//						closestTypes.put(type, new HashMap<Pair<Set<String>>,Double>());
//					} else {
//						line = line.substring(1);
//						String parent = line.substring(0,line.indexOf('\t')).trim();
//						parent = rnames.containsKey(parent) ? rnames.get(parent) : parent;
//						line = line.substring(line.indexOf('\t'+1));
//						String grade = line.substring(0,line.indexOf('\t')).trim();
//						closestTypes.get(type).put(createPair(type, parent),Double.parseDouble(grade));
//					}
//				}
//				input.close();
//				if (DEBUG) System.out.println("\tDone!");
//				return;
//			} catch (Exception e) {
//				e.printStackTrace();
//			}			
//		}
		final FileWriter output;
		try {
			File dir = new File(outputDir+strippedBinary);
			if (!dir.exists()) {
				dir.mkdirs();
			}
			dir = new File(outputDir+strippedBinary+"/"+scorer.getName());
			if (!dir.exists()) {
				dir.mkdirs();
			}
			output = new FileWriter(outputDir+strippedBinary+'/'+scorer.getName()+'/'+binary+".hierarchy"+filenameAddition+".closest");
			for (Set<String> cluster : this.clusters) {
				for (String x : cluster) {
					assert possibleParents.containsKey(x);
					Map<Pair<Set<String>>,Double> pairs = new HashMap<Pair<Set<String>>,Double>();
					for (String y : possibleParents.get(x)) {
						Pair<Set<String>> current = createPair(x,y);
						pairs.put(current, scorer.getScore(current, this));
					}
					List<Pair<Set<String>>> sortedPairs = new LinkedList<Pair<Set<String>>>();
					sortedPairs.addAll(pairs.keySet());
					Collections.sort(sortedPairs, scorer.getComparator(pairs));
					closestTypes.put(x, pairs);
					double variance = getVariance(pairs);
					output.write(names.get(x)+"[ "+variance+" , "+Math.sqrt(variance)+" ]");
					if (parents.containsKey(x) && !possibleParents.get(x).contains(parents.get(x))) {
						output.write(" NP");
					}
					output.write(":\n");
					for (Pair<Set<String>> p : sortedPairs) {
						String tag = "";
						String q = p.query.iterator().next();
						String r = p.reference.iterator().next();
						if (parents.containsKey(q) && parents.get(q).equals(r)) {
							tag = "P";
						}
						if (children.containsKey(q) && children.get(q).contains(r)) {
							tag = "C";
						}
						if (siblings.containsKey(q) && siblings.get(q).contains(r)) {
							tag = "S";
						}
						output.write("\t"+(names.containsKey(r) ? names.get(r) : r)+"\t"+scorer.getNormalizedScore(pairs, p)+"\t\t\t"+tag+"\n");
					}
					output.write("\n");
				}
				output.write(seperator+"\n\n");
				output.flush();
			}
			output.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (DEBUG) System.out.println("\tDone!");
	}
	
	private Set<String> getNodesInSubTree(Node n) {
		Set<String> res = new TreeSet<String>();
		String name = n.getName();
		res.add(rnames.containsKey(name) ? rnames.get(name) : name);
		for(Forest.Node c : n.getChildren()) {
			res.addAll(getNodesInSubTree(c));
		}
		return res;
	}
	
	private Map<String,Set<String>> duplicateParentsMap(Map<String,Set<String>> map) {
		Map<String,Set<String>> dupMap = new HashMap<String, Set<String>>();
		for (String type : map.keySet()) {
			Set<String> parents = new TreeSet<String>();
			parents.addAll(map.get(type));
			dupMap.put(type, parents);
		}
		return dupMap;
	}
	
	private void getAllForests(Map<String,Set<String>> parents, Collection<Forest> res) {
		getAllForestsSingleOptions(duplicateParentsMap(parents), new Forest(), res);
	}
	private void getAllForestsSingleOptions(Map<String,Set<String>> parents, Forest f, Collection<Forest> res) {
		Set<String> added = new TreeSet<String>();	
		boolean redo = true;
		while(redo) {
			redo = false;
			for (String t : parents.keySet()) {
				if (parents.get(t).size() == 0) {
					f.addToForest(names.containsKey(t) ? names.get(t) : t);
					added.add(t);
				}
			}
			for (String t : added) {
				parents.remove(t);
			}
			boolean changed = true;
			while (changed) {
				changed = false;
				added.clear();
				for (String t : parents.keySet()) {
					if (parents.get(t).size() == 1) {
						String parent = parents.get(t).iterator().next();
						if (f.contains(names.containsKey(parent) ? names.get(parent) : parent)) {
							f.addToForest(names.containsKey(t) ? names.get(t) : t, names.containsKey(parent) ? names.get(parent) : parent);
							added.add(t);
							changed = true;
							redo = true;
							Set<String> nodes = getNodesInSubTree(f.getNode(names.containsKey(t) ? names.get(t) : t));
							if (parents.containsKey(parent)) {
								parents.get(parent).removeAll(nodes);
							}
							Forest.Tree tree = f.getTree(names.containsKey(t) ? names.get(t) : t);
							Forest.Node n = tree.getRoot();
							String root = n.getName();
							root = rnames.containsKey(root) ? rnames.get(root) : root;
							if (!root.equals(parent)) {
								if (parents.containsKey(root)) {
									parents.get(root).removeAll(nodes);
								}		
							}						
						}
					}
				}
				for (String t : added) {
					parents.remove(t);
				}
			}
		}
		getAllForestsMultipleOptions(parents, f, res);
	}
	private void getAllForestsMultipleOptions(Map<String,Set<String>> parents, Forest f, Collection<Forest> res) {
		if (parents.isEmpty()) {
			assert f.getNodes().size() == possibleParents.size();
			res.add(f);
			return;
		}
		Set<String> types = new TreeSet<String>();
		Set<String> check = new TreeSet<String>();
		check.add(parents.keySet().iterator().next());
		while (check.size() > 0) {
			String t = check.iterator().next();
			check.remove(t);
			types.add(t);
			for (String p : parents.get(t)) {
				if (parents.containsKey(p)) {
					if (!types.contains(p)) {
						check.add(p);
					}
				}
			}
		}
		Set<Pair<String>> pairs = new HashSet<Pair<String>>();
		for (String type : types) {
			for (String parent : parents.get(type)) {
				Pair<String> pair = new Pair<String>();
				pair.query = type;
				pair.reference = parent;
				pairs.add(pair);
			}
		}
		for (Pair<String> pair : pairs) {
			Map<String,Set<String>> dupParents = duplicateParentsMap(parents);
			Forest dupF = f.duplicate();
			String type = pair.query;
			String parent = pair.reference;
			dupF.addToForest(names.containsKey(type) ? names.get(type) : type, names.containsKey(parent) ? names.get(parent) : parent);
			dupParents.remove(type);
			Set<String> nodes = getNodesInSubTree(dupF.getNode(names.containsKey(type) ? names.get(type) : type));
			if (dupParents.containsKey(parent)) {
				dupParents.get(parent).removeAll(nodes);
			}
			Forest.Tree t = dupF.getTree(names.containsKey(type) ? names.get(type) : type);
			Forest.Node n = t.getRoot();
			String root = n.getName();
			root = rnames.containsKey(root) ? rnames.get(root) : root;
			if (!root.equals(parent)) {
				if (dupParents.containsKey(root)) {
					dupParents.get(root).removeAll(nodes);
				}		
			}
			getAllForestsSingleOptions(dupParents, dupF, res);
		}
	}
	
	private void getGreedyForests(Map<String,Set<String>> parents, Collection<Forest> res) {
		if (DEBUG) System.out.print(printPrefix+"Constructing greedy forests...");
		getGreedyForestsSingleOptions(duplicateParentsMap(parents), new Forest(), res);
		if (DEBUG) System.out.println("\tDone!");
	}
	private void getGreedyForestsSingleOptions(Map<String,Set<String>> parents, Forest f, Collection<Forest> res) {
		Set<String> added = new TreeSet<String>();	
		boolean redo = true;
		while(redo) {
			redo = false;
			for (String t : parents.keySet()) {
				if (parents.get(t).size() == 0) {
					f.addToForest(names.containsKey(t) ? names.get(t) : t);
					added.add(t);
				}
			}
			for (String t : added) {
				parents.remove(t);
			}
			boolean changed = true;
			while (changed) {
				changed = false;
				added.clear();
				for (String t : parents.keySet()) {
					if (parents.get(t).size() == 1) {
						String parent = parents.get(t).iterator().next();
						if (f.contains(names.containsKey(parent) ? names.get(parent) : parent)) {
							f.addToForest(names.containsKey(t) ? names.get(t) : t, names.containsKey(parent) ? names.get(parent) : parent);
							added.add(t);
							changed = true;
							redo = true;
							Set<String> nodes = getNodesInSubTree(f.getNode(names.containsKey(t) ? names.get(t) : t));
							if (parents.containsKey(parent)) {
								parents.get(parent).removeAll(nodes);
							}
							Forest.Tree tree = f.getTree(names.containsKey(t) ? names.get(t) : t);
							Forest.Node n = tree.getRoot();
							String root = n.getName();
							root = rnames.containsKey(root) ? rnames.get(root) : root;
							if (!root.equals(parent)) {
								if (parents.containsKey(root)) {
									parents.get(root).removeAll(nodes);
								}		
							}						
						}
					}
				}
				for (String t : added) {
					parents.remove(t);
				}
			}
		}
		getGreedyForestsMultipleOptions(parents, f, res);
	}
	private void getGreedyForestsMultipleOptions(Map<String,Set<String>> parents, Forest f, Collection<Forest> res) {
		if (parents.isEmpty()) {
			res.add(f);
			return;
		}
		Set<Pair<Set<String>>> pairs = new HashSet<Pair<Set<String>>>();
		Double bestScore = null;
		for (String type : parents.keySet()) {
			Map<Pair<Set<String>>,Double> closest = closestTypes.get(type);
			List<Pair<Set<String>>> sortedClosest = new LinkedList<Pair<Set<String>>>();
			Set<String> posParents = parents.get(type);
			for (Pair<Set<String>> p : closest.keySet()) {
				if (posParents.contains(p.reference.iterator().next())) {
					sortedClosest.add(p);
				}
			}
			Collections.sort(sortedClosest, scorer.getComparator(closest));
			Pair<Set<String>> chosenPair = sortedClosest.get(0);
			Double currentScore = closest.get(chosenPair);
			if (bestScore == null) {
				bestScore = currentScore;
				pairs.add(chosenPair);
			} else {
				if (bestScore.equals(currentScore)) {
					pairs.add(chosenPair);
				} else if (scorer.getValueComparator().compare(bestScore, currentScore) > 0) {
					bestScore = currentScore;
					pairs.clear();
					pairs.add(chosenPair);
				}
			}
		}
		for (Pair<Set<String>> pair : pairs) {
			Map<String,Set<String>> dupParents = duplicateParentsMap(parents);
			Forest dupF = f.duplicate();
			String type = pair.query.iterator().next();
			String parent = pair.reference.iterator().next();
			dupF.addToForest(names.containsKey(type) ? names.get(type) : type, names.containsKey(parent) ? names.get(parent) : parent);
			dupParents.remove(type);
			Set<String> nodes = getNodesInSubTree(dupF.getNode(names.containsKey(type) ? names.get(type) : type));
			if (dupParents.containsKey(parent)) {
				dupParents.get(parent).removeAll(nodes);
			}
			Forest.Tree t = dupF.getTree(names.containsKey(type) ? names.get(type) : type);
			Forest.Node n = t.getRoot();
			String root = n.getName();
			root = rnames.containsKey(root) ? rnames.get(root) : root;
			if (!root.equals(parent)) {
				if (dupParents.containsKey(root)) {
					dupParents.get(root).removeAll(nodes);
				}		
			}
			getGreedyForestsSingleOptions(dupParents, dupF, res);
		}
	}
//	private Forest buildGreedyForest() {
//		if (DEBUG) System.out.print(printPrefix+"Constructing forest...");
//		Map<String,Set<String>> dupPossibleParents = duplicateParentsMap(possibleParents);
//		Forest forest = new Forest();
//		Set<String> removeNodes = new TreeSet<String>();
//		while (dupPossibleParents.keySet().size() > 0) {
//			for (String type : dupPossibleParents.keySet()) {
//				if (dupPossibleParents.get(type).size() == 0) {
//					removeNodes.add(type);
//					if (names.containsKey(type)) {
//						type = names.get(type);
//					}
//					forest.addToForest(type);
//				}
//			}
//			for (String n : removeNodes) {
//				dupPossibleParents.remove(n);
//			}
//			removeNodes.clear();
//			/*
//			// first set all types with only one possible parent
//			boolean pass = false;
//			Set<String> parentNodes = new TreeSet<String>();
//			for (String type : dupPossibleParents.keySet()) {
//				if (!parentNodes.contains(type)) {
//					if (dupPossibleParents.get(type).size() == 1) {
//						String parent = dupPossibleParents.get(type).iterator().next();
//						if ((dupPossibleParents.containsKey(parent)) && (dupPossibleParents.get(parent).size() == 1) && (dupPossibleParents.get(parent).contains(type))) {
//							if (closestTypes.get(type).get(createPair(type, parent)) < closestTypes.get(parent).get(createPair(parent, type))) {
//								parentNodes.add(type);
//								removeNodes.add(type);
//								String tmp = type;
//								type = parent;
//								parent = tmp;
//							}
//						}
//						removeNodes.add(type);
//						forest.addToForest(names.containsKey(type) ? names.get(type) : type, names.containsKey(parent) ? names.get(parent) : parent);
//						if (dupPossibleParents.containsKey(parent)) {
//							dupPossibleParents.get(parent).removeAll(getNodesInTree(forest.getNode(type)));
//						}
//						pass = true;
//					}
//				}
//			}
//			for (String n : removeNodes) {
//				dupPossibleParents.remove(n);
//			}
//			removeNodes.clear();
//			if (pass) {
//				continue; // there maybe new types with 1/0 possible parents
//			}
//			*/
//			// choose most likely pair
//			if (dupPossibleParents.keySet().size() > 0) {
//				Map<Pair<Set<String>>,Double> highestPairs = new HashMap<Pair<Set<String>>,Double>();
//				for (String type : dupPossibleParents.keySet()) {
//					Map<Pair<Set<String>>,Double> pairs = closestTypes.get(type);
//					List<Pair<Set<String>>> sortedPairs = new LinkedList<Pair<Set<String>>>();
//					for (Pair<Set<String>> p : pairs.keySet()) {
//						if (dupPossibleParents.get(type).contains(p.reference.iterator().next())) {
//							sortedPairs.add(p);
//						}
//					}
//					Collections.sort(sortedPairs, scorer.getComparator(pairs));
//					Pair<Set<String>> highestPair = sortedPairs.get(0);
//					highestPairs.put(highestPair, pairs.get(highestPair));
//				}
//				List<Pair<Set<String>>> sortedHighestPairs = new LinkedList<Pair<Set<String>>>();
//				sortedHighestPairs.addAll(highestPairs.keySet());
//				Collections.sort(sortedHighestPairs, scorer.getComparator(highestPairs));
//				Pair<Set<String>> chosen = sortedHighestPairs.get(0);
//				String query = chosen.query.iterator().next();
//				String reference = chosen.reference.iterator().next();
//				forest.addToForest(names.containsKey(query) ? names.get(query) : query, names.containsKey(reference) ? names.get(reference) : reference);
//				Set<String> set = getNodesInSubTree(forest.getNode(names.containsKey(query) ? names.get(query) : query));
//				if (dupPossibleParents.containsKey(reference)) {
//					dupPossibleParents.get(reference).removeAll(set);
//				}
//				String root = forest.getTree(names.containsKey(query) ? names.get(query) : query).getRoot().getName();
//				root = rnames.containsKey(root) ? rnames.get(root) : root;
//				if (dupPossibleParents.containsKey(root)) {
//					dupPossibleParents.get(root).removeAll(set);
//				}
//				dupPossibleParents.remove(query);
//			}
//		}
//		if (DEBUG) System.out.println("\tDone!");
//		return forest;
//	}
	
	public List<Double> findMinimalBaseline() {
		if (DEBUG) System.out.print(printPrefix+"Finding baseline forest...");
		Map<String,Set<String>> dupPossibleParents = duplicateParentsMap(possibleParents);
		for (String type : dupPossibleParents.keySet()) {
			Set<String> pps = dupPossibleParents.get(type);
			if (parents.containsKey(type)) {
				String parent = parents.get(type);
				if (pps.contains(parent)) {
					pps.clear();
					pps.add(parent);
				}
			}
		}
		Collection<Forest> forests = new LinkedList<Forest>();
		getAllForests(dupPossibleParents, forests);
		Map<Forest,Double> forestScores = new HashMap<Forest,Double>();
		Map<Forest,Double> treeScores = new HashMap<Forest,Double>();
		Map<Forest,Double> edgeScores = new HashMap<Forest,Double>();
		for (Forest f : forests) {
			forestScores.put(f,fullForestDistance(f,groundTruthForest));
			treeScores.put(f,singleTreeDistance(f,groundTruthForest));
			edgeScores.put(f,edgesDistance(f,groundTruthForest));
		}
		List<Double> result = new LinkedList<Double>();
		result.add(Collections.min(forestScores.values()));
		result.add(Collections.min(treeScores.values()));
		result.add(Collections.min(edgeScores.values()));
		FileWriter outputForest;
		FileWriter outputTree;
		FileWriter outputEdge;
		try {
			outputForest = new FileWriter(outputDir+strippedBinary+'/'+binary+".baseline.forest");
			outputTree = new FileWriter(outputDir+strippedBinary+'/'+binary+".baseline.tree");
			outputEdge = new FileWriter(outputDir+strippedBinary+'/'+binary+".baseline.edges");
			for (Forest f : forests) {
				if (forestScores.get(f) == result.get(0)) {
					f.asTree().write(outputForest);
					outputForest.write("\n");
				}
				if (treeScores.get(f) == result.get(1)) {
					f.asTree().write(outputTree);
					outputTree.write("\n");
				}
				if (edgeScores.get(f) == result.get(2)) {
					f.asTree().write(outputEdge);
					outputEdge.write("\n");
				}
			}
			outputForest.close();
			outputTree.close();
			outputEdge.close();
		} catch (IOException e) {
			e.printStackTrace();
		}	
		if (DEBUG) System.out.println("\t...Done!");
		return result;
	}
	
	private LblTree convertToLblTree(Node n) {
		LblTree node = new LblTree(n.getName(), -1);
		List<Node> children = n.getChildren();
		Collections.sort(children);
		for (Node c : children) {
			node.add(convertToLblTree(c));
		}
		return node;
	}
	private LblTree convertToLblTree(Forest f) {
		LblTree node = new LblTree(Forest.rootName, -1);
		for (Forest.Tree t : f.getTrees()) {
			node.add(convertToLblTree(t.getRoot()));
		}
		return node;
	}
	private double countFalseEdges(Forest.Node n, Forest f) {
		Forest.Node other = f.getNode(n.getName());
		double score = 0;
		for (Forest.Node c : n.getChildren()) {
			if (!other.getChildren().contains(f.getNode(c.getName()))) {
				++score;
			}
			score += countFalseEdges(c,f);
		}
		return score;
	}
	private double fullForestDistance(Forest f1, Forest f2) {
		double dist = 0;
		for (Forest.Node n1 : f1.getNodes()) {
			Forest.Node n2 = f2.getNode(n1.getName());
			LblTree lbl1 = convertToLblTree(n1);
			LblTree lbl2 = convertToLblTree(n2);
			dist += ted.nonNormalizedTreeDist(lbl1, lbl2);			
		}
		return dist;
	}
	private double singleTreeDistance(Forest f1, Forest f2) {
		LblTree lbl1 = convertToLblTree(f1);
		LblTree lbl2 = convertToLblTree(f2);
		return ted.nonNormalizedTreeDist(lbl1, lbl2);
	}
	private double edgesDistance(Forest f1, Forest f2) {
		double dist = 0;
		Set<String> roots = new TreeSet<String>();
		for (Forest.Tree t : f2.getTrees()) {
			roots.add(t.getRoot().getName());
		}
		for (Forest.Tree t : f1.getTrees()) {
			if (!roots.contains(t.getRoot().getName())) {
				++dist;
			}
			dist += countFalseEdges(t.getRoot(), f2);
		}
		return dist;
	}

	private double sumTreeCost(Forest.Node n) {
		String parent = n.getName();
		parent = rnames.containsKey(parent) ? rnames.get(parent) : parent;
		double score = scorer.getIntialValue();
		for (Forest.Node c : n.getChildren()) {
			String child = c.getName();
			child = rnames.containsKey(child) ? rnames.get(child) : child;
			Double currentScore = scorer.getIntialValue();
			for (Pair<Set<String>> p : closestTypes.get(child).keySet()) {
				if (p.reference.equals(wrapInSet(parent))) {
					currentScore = closestTypes.get(child).get(p);
				}
			}
			score = scorer.calcCombinedScore(score, currentScore);
			score = scorer.calcCombinedScore(score, sumTreeCost(c));
		}
		return score;
	}
	private double sumForestCost(Forest f) {
		double score = scorer.getIntialValue();
		for (Forest.Tree t : f.getTrees()) {
			String root = t.getRoot().getName();
			root = rnames.containsKey(root) ? rnames.get(root) : root;
			//score = scorer.calcCombinedScore(score, scorer.getScoreAsRoot(root, this));
			score = scorer.calcCombinedScore(score, sumTreeCost(t.getRoot()));
		}
		return score;
	}
	private Collection<Forest> getGreedyForests() {
		Collection<Forest> forests = new LinkedList<Forest>();
		getGreedyForests(duplicateParentsMap(possibleParents), forests);
		if (DEBUG) System.out.print(printPrefix+"Ranking greedy forests...");
		Collection<Forest> bestForests = new LinkedList<Forest>();
		Double bestScore = null;
		for (Forest f : forests) {
			Double currentScore = sumForestCost(f);
			if (bestScore == null) {
				bestScore = currentScore;
				bestForests.add(f);
			} else {
				if (bestScore == currentScore) {
					bestForests.add(f);
				} else if (scorer.getValueComparator().compare(bestScore, currentScore) > 0) {
					bestScore = currentScore;
					bestForests.clear();
					bestForests.add(f);
				}
			}
		}
		if (DEBUG) System.out.println("\tDone!");
		return bestForests;
	}
	
	public List<Double> measureGreedyForest() {
		Collection<Forest> forests = getGreedyForests();
		if (DEBUG) System.out.print(printPrefix+"Measuring greedy fores`t distance...");
		Map<Forest,Double> forestDistance = new HashMap<Forest,Double>();
		Map<Forest,Double> treeDistance = new HashMap<Forest,Double>();
		Map<Forest,Double> edgeDistance = new HashMap<Forest,Double>();
		for (Forest f : forests) {
			forestDistance.put(f,fullForestDistance(f, groundTruthForest));
			treeDistance.put(f,singleTreeDistance(f, groundTruthForest));
			edgeDistance.put(f,edgesDistance(f, groundTruthForest));
		}
		List<Double> result = new LinkedList<Double>();
		result.add(Collections.max(forestDistance.values()));
		result.add(Collections.max(treeDistance.values()));
		result.add(Collections.max(edgeDistance.values()));
		result.add(Collections.min(forestDistance.values()));
		result.add(Collections.min(treeDistance.values()));
		result.add(Collections.min(edgeDistance.values()));
		FileWriter outputForest;
		FileWriter outputTree;
		FileWriter outputEdges;
		try {
			outputForest = new FileWriter(outputDir+strippedBinary+'/'+scorer.getName()+'/'+binary+".hierarchy"+filenameAddition+".forest");
			outputTree = new FileWriter(outputDir+strippedBinary+'/'+scorer.getName()+'/'+binary+".hierarchy"+filenameAddition+".tree");
			outputEdges = new FileWriter(outputDir+strippedBinary+'/'+scorer.getName()+'/'+binary+".hierarchy"+filenameAddition+".edges");
			for (Forest f : forests) {
				if (forestDistance.get(f) == result.get(0)) {
					f.asTree().write(outputForest);
					outputForest.write("\n");
				}
				if (treeDistance.get(f) == result.get(1)) {
					f.asTree().write(outputTree);
					outputTree.write("\n");
				}
				if (edgeDistance.get(f) == result.get(2)) {
					f.asTree().write(outputEdges);
					outputEdges.write("\n");
				}
			}
			outputForest.close();
			outputTree.close();
			outputEdges.close();
		} catch (IOException e) {
			e.printStackTrace();
		}	
		if (DEBUG) System.out.println("\tDone!");
		return result;
	}

//	public void checkContainment() { 
//		if (DEBUG) System.out.print(printPrefix+"Checking type containment...");
//		final FileWriter output;
//		try {
//			File dir = new File(outputDir+strippedBinary);
//			if (!dir.exists()) {
//				dir.mkdirs();
//			}
//			dir = new File(outputDir+strippedBinary+"/Containment");
//			if (!dir.exists()) {
//				dir.mkdirs();
//			}
//			output = new FileWriter(outputDir+strippedBinary+"/Containment/"+binary+".hierarchy"+".Q"+querySubs+".R"+refSubs+".containment");
//			output.write("type1\ttype2\tsize1\tsize2\tshared\n");
//			for (Set<String> cluster : this.clusters) {
//				for (String type1: cluster) {
//					for (String type2 : possibleParents.get(type1)) {
//						Collection<ComparableStringArray> codes1 = getQueryCodes(wrapInSet(type1));
//						Collection<ComparableStringArray> codes2 = getRefCodes(wrapInSet(type2));
//						int count = 0;
//						for (ComparableStringArray i : codes1) {
//							if (codes2.contains(i)) {
//								++count;
//							}
//						}
//						output.write(names.get(type1)+"\t"+names.get(type2)+"\t"+codes1.size()+"\t"+codes2.size()+"\t"+count+"\n");
//				}
//					output.write("\n");
//					output.flush();
//				}
//			}
//			output.close();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}		
//		if (DEBUG) System.out.println("\tDone!");
//	}

	protected static class HierarchyClosestRunnable implements Runnable {

		private String b;
		private boolean q;
		private boolean r;
		private PairScore s;
		private int n;
		private String p;
		private Map<String, Double> maxForestDistances;
		private Map<String, Double> maxTreeDistances;
		private Map<String, Double> maxEdgeDistances;
		private Map<String, Double> minForestDistances;
		private Map<String, Double> minTreeDistances;
		private Map<String, Double> minEdgeDistances;
		
		private String desc;
		
		public HierarchyClosestRunnable(String b, boolean q, boolean r, PairScore s, int n, String p, String desc, Map<String, Double> maxForestDistances, Map<String, Double> maxTreeDistances, Map<String, Double> maxEdgeDistances, Map<String, Double> minForestDistances, Map<String, Double> minTreeDistances, Map<String, Double> minEdgeDistances) {
			this.b = b;
			this.q = q;
			this.r = r;
			this.s = s;
			this.n = n;
			this.p = p;
			this.desc = desc;
			this.maxForestDistances = maxForestDistances;
			this.maxTreeDistances = maxTreeDistances;
			this.maxEdgeDistances = maxEdgeDistances;
			this.minForestDistances = minForestDistances;
			this.minTreeDistances = minTreeDistances;
			this.minEdgeDistances = minEdgeDistances;
		}
		
		@Override
		public void run() {
			System.out.println("Started: "+desc);
			long start = System.currentTimeMillis();
			HierarchyBuilder hb = new HierarchyBuilder(b, q, r, s, n, p);
			hb.findClosest();
			List<Double> distance = hb.measureGreedyForest();
			maxForestDistances.put(desc, distance.get(0));
			maxTreeDistances.put(desc, distance.get(1));
			maxEdgeDistances.put(desc, distance.get(2));
			minForestDistances.put(desc, distance.get(3));
			minTreeDistances.put(desc, distance.get(4));
			minEdgeDistances.put(desc, distance.get(5));
			long end = System.currentTimeMillis();
			System.out.println("Finished: "+desc+"\tMax Distance(forest,tree,edge)="+distance.subList(0, 3)+"\tMin Distance(forest,tree,edge)="+distance.subList(3, 6)+"\texecution time: "+(end-start)+"\n");
		}
		
	}
	
//	protected static class HierarchyContainmentRunnable implements Runnable {
//
//		private String b;
//		private boolean q;
//		private boolean r;
//		private PairScore s;
//		private int n;
//		private String p;
//		
//		private String desc;
//		
//		public HierarchyContainmentRunnable(String b, boolean q, boolean r, PairScore s, int n, String p, String desc) {
//			this.b = b;
//			this.q = q;
//			this.r = r;
//			this.s = s;
//			this.n = n;
//			this.p = p;
//			this.desc = desc;
//		}
//		
//		@Override
//		public void run() {
//			System.out.println("Started: "+desc);
//			long start = System.currentTimeMillis();
//			HierarchyBuilder hb = new HierarchyBuilder(b, q, r, s, n, p);
//			hb.checkContainment();
//			long end = System.currentTimeMillis();
//			System.out.println("Finished: "+desc+"\n\texecution time: "+(end-start)+"\n");
//		}
//		
//	}
	
	private static void deleteFolder(File folder) {
	    File[] files = folder.listFiles();
	    if(files!=null) { //some JVMs return null for empty dirs
	        for(File f: files) {
	            if(f.isDirectory()) {
	                deleteFolder(f);
	            } else {
	                f.delete();
	            }
	        }
	    }
	    folder.delete();
	}
	private static void printUsage() {
		System.err.println("Usage: java -jar hierarchies.jar <binary name>");
		System.err.println("   Or: java -ea -classpath .:<path to mongo jar> vmm.HierarchyBuilder <binary name>");
		System.err.println();
	}
	public static void main_closest_dist(String binary, ConcurrentMap<String, Double> maxForestDistances, ConcurrentMap<String, Double> maxTreeDistances, ConcurrentMap<String, Double> maxEdgeDistances, ConcurrentMap<String, Double> minForestDistances, ConcurrentMap<String, Double> minTreeDistances, ConcurrentMap<String, Double> minEdgeDistances, ExecutorService es) {
		// Distances
		for (int q=0; q<=1; ++q) {
			for (int r=0; r<=1; ++r) {
				String desc = binary+"\tDistances\tQSubs="+(q==0)+"\tRSubs="+(r==0);
				if (SERIAL) {
					new HierarchyClosestRunnable(binary, (q==0), (r==0), new DistanceScorer(), 1, "\t", desc, maxForestDistances, maxTreeDistances, maxEdgeDistances, minForestDistances, minTreeDistances, minEdgeDistances).run();
				} else {
					es.submit(new HierarchyClosestRunnable(binary, (q==0), (r==0), new DistanceScorer(), 1, "\t", desc, maxForestDistances, maxTreeDistances, maxEdgeDistances, minForestDistances, minTreeDistances, minEdgeDistances));
				}
			}
		}
	}
	public static void main_closest_prob(String binary, ConcurrentMap<String, Double> maxForestDistances, ConcurrentMap<String, Double> maxTreeDistances, ConcurrentMap<String, Double> maxEdgeDistances, ConcurrentMap<String, Double> minForestDistances, ConcurrentMap<String, Double> minTreeDistances, ConcurrentMap<String, Double> minEdgeDistances, ExecutorService es) {
		// Probabilities
		for (int q=0; q<=1; ++q) {
			for (int r=0; r<=1; ++r) {
				for (int l=0; l<=1; ++l) {
					int repeat = (int) Math.pow(10, l);
					String desc = binary+"\tProbabilities\tRepeat="+repeat+"\tQSubs="+(q==0)+"\tRSubs="+(r==0);
					if (SERIAL) {
						new HierarchyClosestRunnable(binary, (q==0), (r==0), new ProbabilityScorer(), repeat, "\t", desc, maxForestDistances, maxTreeDistances, maxEdgeDistances, minForestDistances, minTreeDistances, minEdgeDistances).run();
					} else {
						es.submit(new HierarchyClosestRunnable(binary, (q==0), (r==0), new ProbabilityScorer(), repeat, "\t", desc, maxForestDistances, maxTreeDistances, maxEdgeDistances, minForestDistances, minTreeDistances, minEdgeDistances));
					}
				}
			}
		}
	}
	public static void main_closest_jsd(String binary, ConcurrentMap<String,Double> maxForestDistances, ConcurrentMap<String, Double> maxTreeDistances, ConcurrentMap<String, Double> maxEdgeDistances, ConcurrentMap<String, Double> minForestDistances, ConcurrentMap<String, Double> minTreeDistances, ConcurrentMap<String, Double> minEdgeDistances, ExecutorService es) {
		// JSD
		for (int q=0; q<=1; ++q) {
			for (int r=0; r<=1; ++r) {
				for (int l=0; l<=1; ++l) {
					for (SeqsEnum s : JSDScorer.SeqsEnum.values()) {
						int repeat = (int) Math.pow(10, l);
						String desc = binary+"\tJSD\tSeqs="+s.name().substring(0, 1)+"\tRepeat="+repeat+"\tQSubs="+(q==0)+"\tRSubs="+(r==0);
						if (SERIAL) {
							new HierarchyClosestRunnable(binary, (q==0), (r==0), new JSDScorer(s), repeat, "\t", desc, maxForestDistances, maxTreeDistances, maxEdgeDistances, minForestDistances, minTreeDistances, minEdgeDistances).run();
						} else {
							es.submit(new HierarchyClosestRunnable(binary, (q==0), (r==0), new JSDScorer(s), repeat, "\t", desc, maxForestDistances, maxTreeDistances, maxEdgeDistances, minForestDistances, minTreeDistances, minEdgeDistances));
						}
					}
				}
			}
		}
	}
	public static void main_closest_jsd_sqrt(String binary, ConcurrentMap<String,Double> maxForestDistances, ConcurrentMap<String, Double> maxTreeDistances, ConcurrentMap<String, Double> maxEdgeDistances, ConcurrentMap<String, Double> minForestDistances, ConcurrentMap<String, Double> minTreeDistances, ConcurrentMap<String, Double> minEdgeDistances, ExecutorService es) {
		// JSD sqrt
		for (int q=0; q<=1; ++q) {
			for (int r=0; r<=1; ++r) {
				for (int l=0; l<=1; ++l) {
					for (SeqsEnum s : JSDScorer.SeqsEnum.values()) {
						int repeat = (int) Math.pow(10, l);
						String desc = binary+"\tJSD_sqrt\tSeqs="+s.name().substring(0, 1)+"\tRepeat="+repeat+"\tQSubs="+(q==0)+"\tRSubs="+(r==0);
						if (SERIAL) {
							new HierarchyClosestRunnable(binary, (q==0), (r==0), new JSDSqrtScorer(s), repeat, "\t", desc, maxForestDistances, maxTreeDistances, maxEdgeDistances, minForestDistances, minTreeDistances, minEdgeDistances).run();
						} else {
							es.submit(new HierarchyClosestRunnable(binary, (q==0), (r==0), new JSDSqrtScorer(s), repeat, "\t", desc, maxForestDistances, maxTreeDistances, maxEdgeDistances, minForestDistances, minTreeDistances, minEdgeDistances));
						}
					}
				}
			}
		}
	}
	public static void main_closest_dkl(String binary, ConcurrentMap<String,Double> maxForestDistances, ConcurrentMap<String, Double> maxTreeDistances, ConcurrentMap<String, Double> maxEdgeDistances, ConcurrentMap<String, Double> minForestDistances, ConcurrentMap<String, Double> minTreeDistances, ConcurrentMap<String, Double> minEdgeDistances, ExecutorService es) {
		// DKL
		for (int q=0; q<=1; ++q) {
			for (int r=0; r<=1; ++r) {
				for (int l=0; l<=1; ++l) {
					for (int i = 0; i < DKLScorer.SeqsEnum.values().length; ++i) {
						SeqsEnum s = SeqsEnum.values()[i];
						int repeat = (int) Math.pow(10, l);
						String desc = binary+"\tDKL\tSeqs="+s.name().substring(0, 1)+"\tRepeat="+repeat+"\tQSubs="+(q==0)+"\tRSubs="+(r==0);
						if (SERIAL) {
							new HierarchyClosestRunnable(binary, (q==0), (r==0), new DKLScorer(s), repeat, "\t", desc, maxForestDistances, maxTreeDistances, maxEdgeDistances, minForestDistances, minTreeDistances, minEdgeDistances).run();
						} else {
							es.submit(new HierarchyClosestRunnable(binary, (q==0), (r==0), new DKLScorer(s), repeat, "\t", desc, maxForestDistances, maxTreeDistances, maxEdgeDistances, minForestDistances, minTreeDistances, minEdgeDistances));
						}
					}
				}
			}
		}
	}
//	public static void main_containment(String binary, ExecutorService es) {
//		for (int q=0; q<=1; ++q) {
//			for (int r=0; r<=1; ++r) {
//				String desc = binary+"\tContainment\tQSubs="+(q==0)+"\tRSubs="+(r==0);
//				if (SERIAL) {
//					new HierarchyContainmentRunnable(binary, (q==0), (r==0), new DistanceScorer(), 1, "\t", desc).run();
//				} else {
//					es.submit(new HierarchyContainmentRunnable(binary, (q==0), (r==0), new DistanceScorer(), 1, "\t", desc));
//				}
//			}	
//		}
//	}
	public static void main(String[] args) {
		if (args.length < 1) {
			printUsage();
		}
		String b = args[0];
		String sb = b.substring(0,b.lastIndexOf('.'));
		int numCores = Runtime.getRuntime().availableProcessors();///2;
		ExecutorService es = Executors.newFixedThreadPool(numCores);
		System.out.println("Running on binary "+b+" (running on "+numCores+" cores)\n");
		deleteFolder(new File(outputDir+sb));
		final ConcurrentMap<String, Double> maxForestDistances = new ConcurrentHashMap<String, Double>();
		final ConcurrentMap<String, Double> maxTreeDistances = new ConcurrentHashMap<String, Double>();
		final ConcurrentMap<String, Double> maxEdgeDistances = new ConcurrentHashMap<String, Double>();
		final ConcurrentMap<String, Double> minForestDistances = new ConcurrentHashMap<String, Double>();
		final ConcurrentMap<String, Double> minTreeDistances = new ConcurrentHashMap<String, Double>();
		final ConcurrentMap<String, Double> minEdgeDistances = new ConcurrentHashMap<String, Double>();
//		main_closest_dist(b, maxForestDistances, maxTreeDistances, minForestDistances, minTreeDistances, es);
		main_closest_prob(b, maxForestDistances, maxTreeDistances, maxEdgeDistances, minForestDistances, minTreeDistances, minEdgeDistances, es);
		main_closest_dkl(b, maxForestDistances, maxTreeDistances, maxEdgeDistances, minForestDistances, minTreeDistances, minEdgeDistances, es);
		main_closest_jsd(b, maxForestDistances, maxTreeDistances, maxEdgeDistances, minForestDistances, minTreeDistances, minEdgeDistances, es);
		main_closest_jsd_sqrt(b, maxForestDistances, maxTreeDistances, maxEdgeDistances, minForestDistances, minTreeDistances, minEdgeDistances, es);
//		main_containment(b, es);
		HierarchyBuilder hb = new HierarchyBuilder(b, false, false, new DummyScorer(), "");
		hb.findClosest();
		List<Double> baseline = hb.findMinimalBaseline();
		FileWriter outputGT;
		try {
			outputGT = new FileWriter(outputDir+hb.strippedBinary+'/'+hb.binary+".groundtruth");
			hb.groundTruthForest.asTree().write(outputGT);
			outputGT.close();
		} catch (IOException e) {
			e.printStackTrace();
		}		
		es.shutdown();
		try {
			if (!es.awaitTermination(7, TimeUnit.DAYS)) {
				System.err.println("*** Execution took longer than 7 days ***");
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		try {
			final FileWriter output = new FileWriter(outputDir+sb+"/"+b+".stats");
			output.write("Baseline:\tForest="+baseline.get(0)+"\tSingleTree="+baseline.get(1)+"\tEdges="+baseline.get(2)+"\n");
			List<String> sortedDescs = new LinkedList<String>();
			sortedDescs.addAll(maxEdgeDistances.keySet());
			Collections.sort(sortedDescs, new Comparator<String>() {
				@Override
				public int compare(String o1, String o2) {
					Double d1 = maxEdgeDistances.get(o1);
					Double d2 = maxEdgeDistances.get(o2);
					return d1.compareTo(d2);
				}
			});
			for (String desc : sortedDescs) {
				output.write(desc+"\t"+maxForestDistances.get(desc)+"\t"+maxTreeDistances.get(desc)+"\t"+maxEdgeDistances.get(desc)+"\t"+minForestDistances.get(desc)+"\t"+minTreeDistances.get(desc)+"\t"+minEdgeDistances.get(desc)+"\n");
			}
			output.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}

