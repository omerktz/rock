package vmm;

import java.util.Set;

public class JSDSqrtScorer extends JSDScorer {

	public JSDSqrtScorer(SeqsEnum se) {
		super(se);
	}

	@Override
	public double getScore(Pair<Set<String>> types, HierarchyBuilder dataSource) {
		return Math.sqrt(super.getScore(types, dataSource));
	}

	@Override
	public String getName() {
		return "JSDsqrt";
	}

}
