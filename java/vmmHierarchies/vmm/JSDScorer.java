package vmm;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import vmm.HierarchyBuilder.ComparableStringArray;

public class JSDScorer extends DKLScorer {

	public JSDScorer(SeqsEnum se) {
		super(se);
	}

	private double JSD(Set<String> x, Set<String> y, List<Collection<ComparableStringArray>> seqs, ModelUtils mu) {
		Set<String> xy = new TreeSet<String>();
		xy.addAll(x);
		xy.addAll(y);
		return Math.abs(0.5*(DKL(x,xy,seqs.get(0),mu)+DKL(y,xy,seqs.get(1),mu)));
	}
	
	@Override
	public double getScore(Pair<Set<String>> types, HierarchyBuilder dataSource) {
		ModelUtils mu = dataSource.modelUtils;
		Collection<ComparableStringArray> tmpSeqs = null;
		List<Collection<ComparableStringArray>> seqs = new LinkedList<Collection<ComparableStringArray>>();
//		if (se.equals(SeqsEnum.Query)) {
//			tmpSeqs = dataSource.getQueryCodes(types.query);
//			seqs.add(tmpSeqs);
//			seqs.add(tmpSeqs);
//		}
//		if (se.equals(SeqsEnum.Reference)) {
//			tmpSeqs = dataSource.getRefCodes(types.reference);
//			seqs.add(tmpSeqs);
//			seqs.add(tmpSeqs);
//		}
		if (se.equals(SeqsEnum.Both)) {
			tmpSeqs = dataSource.getRefCodes(types.reference);
			tmpSeqs.addAll(dataSource.getQueryCodes(types.query));
			seqs.add(tmpSeqs);
			seqs.add(tmpSeqs);
		}
		if (se.equals(SeqsEnum.All)) {
			tmpSeqs = collectAllSeqs(dataSource);
			seqs.add(tmpSeqs);
			seqs.add(tmpSeqs);
		}
		if (se.equals(SeqsEnum.Mixed)) {
			seqs.add(dataSource.getQueryCodes(types.query));
			seqs.add(dataSource.getQueryCodes(types.reference));
		}
		return JSD(types.query, types.reference, seqs, mu);
	}

	@Override
	public String getName() {
		return "JSD";
	}

}
