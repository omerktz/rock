package vmm;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import vmm.API.VMMType;
import vmm.HierarchyBuilder.ComparableStringArray;
import vmm.pred.VMMPredictor;

public class ModelUtils {
	
	private final Map<Set<String>,VMMPredictor> predictors;
	private final int alphabetSize;
	private final int maxSeqLength;
	private final HierarchyBuilder dataSource;
	private final int repeatLearn;

	public ModelUtils(int alphabetSize, int maxSeqLength, HierarchyBuilder dataSource, int repeatLearn) {
		this.predictors = new HashMap<Set<String>,VMMPredictor>();
		this.alphabetSize = alphabetSize;
		this.maxSeqLength = maxSeqLength;
		this.dataSource = dataSource;
		this.repeatLearn = repeatLearn;
	}
	
	public VMMPredictor getNewPredictor() {
		return API.getNewPredictor(VMMType.PPMC, alphabetSize, maxSeqLength);
	}
		
	private void createPredictors(Set<String> types) {
		final VMMPredictor predictor = getNewPredictor();
		for (ComparableStringArray seq: dataSource.getRefCodes(types)) {
			for (int i = 0; i < repeatLearn; i++)
				API.trainModel(predictor, seq.data);
				//predictor.learn(seq);
		}
		predictors.put(types, predictor);			
	}
	
	public VMMPredictor getPredictor(Set<String> types) {
		if (!predictors.containsKey(types)) {
			createPredictors(types);
		}
		return predictors.get(types);
	}
	
	protected int getNumRepeatLearn() {
		return repeatLearn;
	}

}
