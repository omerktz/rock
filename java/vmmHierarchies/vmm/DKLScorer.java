package vmm;

import java.util.Collection;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
//import java.util.TreeMap;
//import java.util.TreeSet;

import vmm.HierarchyBuilder.ComparableStringArray;
import vmm.pred.VMMPredictor;

public class DKLScorer implements PairScore {

	public enum SeqsEnum {
		Mixed,
		Both,
		All,
		//Query,
		//Reference,
	};
	protected final SeqsEnum se;
	
	private Collection<ComparableStringArray> allSeqs;
	
	public DKLScorer(SeqsEnum se) {
		this.se = se;
	}

	private static final double NEGTIVE_INVERSE_LOG_2 = - (1 / Math.log(2.0));
	protected double DKL(Set<String> x, Set<String> y, Collection<ComparableStringArray> seqs, ModelUtils mu) {
		VMMPredictor Mx = mu.getPredictor(x);
		VMMPredictor My = mu.getPredictor(y);
		List<Double> p = new LinkedList<Double>();
		List<Double> q = new LinkedList<Double>();
		int totalLength = 0;
		for (ComparableStringArray seq: seqs) {
			p.add(API.logEval(Mx, seq.data));
			q.add(API.logEval(My, seq.data));
			totalLength += seq.data.length;
		}
		double result = 0;
		for (int i=0; i<p.size(); i++) {
			result += (Math.exp(p.get(i)/NEGTIVE_INVERSE_LOG_2)*(p.get(i) - q.get(i)));
		}
		result = result/totalLength; // normalize for entropy per character
		return Math.abs(result);
	}
	
	protected Collection<ComparableStringArray> collectAllSeqs(HierarchyBuilder dataSource) {
		if (allSeqs == null) {
			allSeqs = new LinkedList<ComparableStringArray>();
			for (String path: dataSource.getAllPaths(dataSource.allTypes)) {
				allSeqs.addAll(dataSource.path2code.get(path));
			}
		}
		return allSeqs;
	}
	@Override
	public double getScore(Pair<Set<String>> types, HierarchyBuilder dataSource) {
		ModelUtils mu = dataSource.modelUtils;
		Collection<ComparableStringArray> seqs = new LinkedList<ComparableStringArray>();
//		if (se.equals(SeqsEnum.Query)) {
//			seqs = dataSource.getQueryCodes(types.query);
//		}
//		if (se.equals(SeqsEnum.Reference)) {
//			seqs = dataSource.getRefCodes(types.reference);
//		}
		if (se.equals(SeqsEnum.Both)) {
			seqs = dataSource.getRefCodes(types.reference);
			seqs.addAll(dataSource.getQueryCodes(types.query));
		}
		if (se.equals(SeqsEnum.All)) {
			seqs = collectAllSeqs(dataSource);
		}
		if (se.equals(SeqsEnum.Mixed)) {
			seqs = dataSource.getQueryCodes(types.query);
		}
		return DKL(types.query, types.reference, seqs, mu);
	}

	@Override
	public Comparator<Pair<Set<String>>> getComparator(Map<Pair<Set<String>>, Double> values) {
		return new ValueMin2MaxComparator(values);
	}

	@Override
	public String getFilenameAddition() {
		String res = "."+getName()+"_";
//		if (se.equals(SeqsEnum.Query)) {
//			res += "Q";
//		}
//		if (se.equals(SeqsEnum.Reference)) {
//			res += "R";
//		}
		if (se.equals(SeqsEnum.Both)) {
			res += "B";
		}
		if (se.equals(SeqsEnum.All)) {
			res += "A";
		}
		if (se.equals(SeqsEnum.Mixed)) {
			res += "M";
		}
		return res;
	}

	@Override
	public double getNormalizedScore(Map<Pair<Set<String>>, Double> pairs, Pair<Set<String>> p) {
		return pairs.get(p);
		//return new MeanNormalizer(pairs.values()).normalize(pairs.get(p));
	}

	@Override
	public String getName() {
		return "DKL";
	}

	@Override
	public double calcCombinedScore(double d1, double d2) {
		return d1+d2;
	}
	@Override
	public Comparator<Double> getValueComparator() {
		return new DoubleValueMin2MaxComparator();
	}

//	Map<String,Double> rootCache = new TreeMap<String,Double>();
//	@Override
//	public double getScoreAsRoot(String type, HierarchyBuilder dataSource) {
//		if (!rootCache.containsKey(type)) {
//			Pair<Set<String>> types = new Pair<Set<String>>();
//			types.query = new TreeSet<String>();
//			types.query.add(type);
//			types.reference = dataSource.allTypes;
//			rootCache.put(type, getScore(types, dataSource));
//		}
//		return rootCache.get(type);
//	}

	@Override
	public double getIntialValue() {
		return 0;
	}

}
