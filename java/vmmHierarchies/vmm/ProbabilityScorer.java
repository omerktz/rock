package vmm;

import java.util.Collection;
import java.util.Comparator;
import java.util.Map;
import java.util.Set;
//import java.util.TreeMap;
//import java.util.TreeSet;

import vmm.HierarchyBuilder.ComparableStringArray;
import vmm.pred.VMMPredictor;

public class ProbabilityScorer implements PairScore {

	@Override
	public double getScore(Pair<Set<String>> types, HierarchyBuilder dataSource) {
		VMMPredictor p = dataSource.modelUtils.getPredictor(types.reference);
		Collection<ComparableStringArray> codes = dataSource.getQueryCodes(types.query);
		double score = 0;
		for (ComparableStringArray c : codes) {
			score += API.logEval(p,c.data);
		}
		return score;
	}
	
	@Override
	public Comparator<Pair<Set<String>>> getComparator(Map<Pair<Set<String>>, Double> values) {
		//return new valueMax2MinComparator(values);
		return new ValueMin2MaxComparator(values);
	}

	@Override
	public String getFilenameAddition() {
		return ".Prob";
	}

	@Override
	public double getNormalizedScore(Map<Pair<Set<String>>, Double> pairs, Pair<Set<String>> p) {
		return pairs.get(p);
		//return new MeanNormalizer(pairs.values()).normalize(pairs.get(p));
	}

	@Override
	public String getName() {
		return "Probabilities";
	}

	@Override
	public double calcCombinedScore(double d1, double d2) {
		return d1*d2;
	}

	@Override
	public Comparator<Double> getValueComparator() {
		return new DoubleValueMin2MaxComparator();
	}

//	Map<String,Double> rootCache = new TreeMap<String,Double>();
//	@Override
//	public double getScoreAsRoot(String type, HierarchyBuilder dataSource) {
//		if (!rootCache.containsKey(type)) {
//			Pair<Set<String>> types = new Pair<Set<String>>();
//			types.query = new TreeSet<String>();
//			types.query.add(type);
//			types.reference = dataSource.allTypes;
//			rootCache.put(type, getScore(types, dataSource));
//		}
//		return rootCache.get(type);
//	}

	@Override
	public double getIntialValue() {
		return 1;
	}

}
