package vmm;

import java.util.Collection;
import java.util.Comparator;
import java.util.Map;
import java.util.Set;


public interface PairScore {
	
	public class Pair<T> {
		public T query;
		public T reference;
		
		@Override
		public int hashCode() {
			return ("("+query.toString()+","+reference.toString()+")").hashCode();
		}
	}

	public class ValueMin2MaxComparator implements Comparator<Pair<Set<String>>> {
		private Map<Pair<Set<String>>, Double> values;
		public ValueMin2MaxComparator(Map<Pair<Set<String>>,Double> vals) {
			this.values = vals;
		}
		@Override
		public int compare(Pair<Set<String>> arg0, Pair<Set<String>> arg1) {
			Double val0 = values.get(arg0);
			Double val1 = values.get(arg1);
			return val0.compareTo(val1);
		}
	}

	public class ValueMax2MinComparator implements Comparator<Pair<Set<String>>> {
		private ValueMin2MaxComparator min2max;
		public ValueMax2MinComparator(Map<Pair<Set<String>>,Double> vals) {
			this.min2max = new ValueMin2MaxComparator(vals);
		}
		@Override
		public int compare(Pair<Set<String>> arg0, Pair<Set<String>> arg1) {
			return -this.min2max.compare(arg0, arg1);
		}
	}
	
	public double getScore(Pair<Set<String>> types, HierarchyBuilder dataSource);
	
	public Comparator<Pair<Set<String>>> getComparator(Map<Pair<Set<String>>,Double> values);

	public String getFilenameAddition();
	
	public class MeanNormalizer {
		private final double mean;
		public MeanNormalizer(Collection<Double> data) {
			double sum = 0;
			for (Double d : data) {
				sum += d.doubleValue();
			}
			mean = sum/((double)data.size());
		}
		public double normalize(double d) {
			return d/mean;
		}
	}
	
	public double getNormalizedScore(Map<Pair<Set<String>>,Double> pairs, Pair<Set<String>> p);
	
	public String getName();
	
	public double calcCombinedScore(double d1, double d2);
	
	public Comparator<Double> getValueComparator();
	
	public class DoubleValueMin2MaxComparator implements Comparator<Double> {
		@Override
		public int compare(Double arg0, Double arg1) {
			return arg0.compareTo(arg1);
		}
	}

	public class DoubleValueMax2MinComparator implements Comparator<Double> {
		private DoubleValueMin2MaxComparator min2max;
		public DoubleValueMax2MinComparator() {
			this.min2max = new DoubleValueMin2MaxComparator();
		}
		@Override
		public int compare(Double arg0, Double arg1) {
			return -this.min2max.compare(arg0, arg1);
		}
	}
	
	//public double getScoreAsRoot(String type, HierarchyBuilder dataSource);

	public double getIntialValue();
}
