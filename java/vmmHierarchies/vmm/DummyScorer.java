package vmm;

import java.util.Comparator;
import java.util.Map;
import java.util.Set;

public class DummyScorer implements PairScore {

	@Override
	public double getScore(Pair<Set<String>> types, HierarchyBuilder dataSource) {
		return 0;	
	}

	@Override
	public Comparator<Pair<Set<String>>> getComparator(Map<Pair<Set<String>>,Double> values) {
		return new ValueMin2MaxComparator(values);
	}
	@Override
	public String getFilenameAddition() {
		return ".Dummy";
	}

	@Override
	public double getNormalizedScore(Map<Pair<Set<String>>, Double> pairs, Pair<Set<String>> p) {
		return pairs.get(p);
	}
	@Override
	public String getName() {
		return "Dummy";
	}
	
	@Override
	public double calcCombinedScore(double d1, double d2) {
		return d1+d2;
	}
	@Override
	public Comparator<Double> getValueComparator() {
		return new DoubleValueMin2MaxComparator();
	}
//	@Override
//	public double getScoreAsRoot(String type, HierarchyBuilder dataSource) {
//		return 0;	
//	}
	@Override
	public double getIntialValue() {
		return 0;
	}

}
