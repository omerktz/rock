package vmm;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

public class Forest {

	public static class Node implements Comparable<Node>{
		private String name;
		private List<Node> children;
		
		protected Node(String name) {
			this.name = name;
			this.children = new LinkedList<Node>();
		}
		
		public void addChild(Node c) {
			this.children.add(c);
		}
		
		public List<Node> getChildren() {
			return this.children;
		}
		
		public String getName() {
			return this.name;
		}
		
		@Override
		public boolean equals(Object obj) {
			if (obj.getClass().equals(Node.class)) {
				return this.name.equals(((Node)obj).name);
			}
			return false;
		}
		
		@Override
		public int hashCode() {
			return name.hashCode();
		}
		
		@Override
		public String toString() {
			return name;
		}
		
		public void print() {
			print(0);
		}
		public void print(int indent) {
			for (int i=0; i < indent; ++i) {
				System.out.print("\t");
			}
			System.out.println(name);
			for (Node c : children) {
				c.print(indent+1);
			}
		}

		public void write(FileWriter output) {
			write(0, output);
		}
		public void write(int indent,FileWriter output) {
			try {
				for (int i=0; i < indent; ++i) {
					output.write("\t");
				}
				output.write(name+"\n");
				for (Node c : children) {
					c.write(indent+1, output);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		@Override
		public int compareTo(Node o) {
			return name.compareTo(o.name);
		}
		
		public Node duplicate() {
			return new Node(this.name);
		}
	}

	public static class Tree implements Comparable<Tree>{
		
		private Node root;
		private Collection<Node> nodes;
		
		public Tree(Node r) {
			this.root = r;
			this.nodes = new TreeSet<Node>();
			nodes.add(r);
		}
		
		public boolean contains(Node n) {
			return this.nodes.contains(n);
		}
		
		public Collection<Node> getNodes() {
			return this.nodes;
		}
		
		public Node getRoot() {
			return this.root;
		}
		
		public boolean addToTree(Node node, Node parent) {
			boolean hasP = nodes.contains(parent);
			boolean hasN = nodes.contains(node);
			if (!hasP && !hasN) {
				return false;
			}
			if (hasP && hasN) {
				return parent.getChildren().contains(node);
			}
			parent.getChildren().add(node);
			nodes.add(parent);
			nodes.add(node);
			if (hasN) {
				if (this.root.equals(node)) {
					this.root = parent;
				}
			}
			if (nodes.size() != countTree(root)) {
				print();
				System.out.println(node.name+" "+parent.name);
				try{ throw new Exception(); } catch (Exception e) { e.printStackTrace(); }
				System.exit(2);
			}
			return true;
		}
		
		public boolean uniteTree(Tree tree, Node parent) {
			if (!this.nodes.contains(parent)) {
				if (nodes.size() != countTree(root)) {
					try{ throw new Exception(); } catch (Exception e) { e.printStackTrace(); }
					System.exit(2);
				}
				return false;
			}
			parent.addChild(tree.root);
			this.nodes.addAll(tree.getNodes());
			if (nodes.size() != countTree(root)) {
				try{ throw new Exception(); } catch (Exception e) { e.printStackTrace(); }
				System.exit(2);
			}
			return true;
		}

		@Override
		public boolean equals(Object obj) {
			if (obj.getClass().equals(Tree.class)) {
				return this.root.equals(((Tree)obj).root);
			}
			return false;
		}
		
		@Override
		public int hashCode() {
			return root.hashCode();
		}
		
		public void print() {
			root.print();
		}

		public void write(FileWriter output) {
			root.write(output);
		}

		@Override
		public int compareTo(Tree o) {
			return root.compareTo(o.root);
		}
		
		private int countTree(Forest.Node r) {
			int res = 1;
			for (Forest.Node c : r.getChildren()) {
				res += countTree(c);
			}
			return res;
		}
		public Tree duplicate() {
			//TODO check
			Map<String,Node> newNodes = new TreeMap<String, Node>();
			Set<Node> copy = new TreeSet<Node>();
			Node current = root.duplicate();
			newNodes.put(root.name, current);
			Tree t = new Tree(current);
			for (Node n : root.children) {
				Node child = n.duplicate();
				newNodes.put(n.name,child);
				t.addToTree(child, current);
				copy.add(n);
			}
			while (copy.size() > 0) {
				current = copy.iterator().next();
				copy.remove(current);
				for (Node n : current.children) {
					Node child = n.duplicate();
					newNodes.put(n.name,child);
					t.addToTree(child, newNodes.get(current.name));
					copy.add(n);
				}
			}
			if (nodes.size() != countTree(root)) {
				try{ throw new Exception(); } catch (Exception e) { e.printStackTrace(); }
				System.exit(2);
			}
			return t;
		}
	}
	
	private Collection<Tree> trees;
	private Map<String, Node> nodesMap;
	
	public Forest() {
		this.trees = new TreeSet<Tree>();
		this.nodesMap = new HashMap<String, Node>();
	}
	
	public Node getNode(String name) {
		if (!nodesMap.containsKey(name)) {
			nodesMap.put(name, new Node(name));
		}
		return nodesMap.get(name);
	}
	
	public Tree getTree(String name) {
		return getTree(getNode(name));
	}
	public Tree getTree(Node n) {
		for (Tree t : trees) {
			if (t.contains(n)) {
				return t;
			}
		}
		return null;
	}
	
	public boolean addToForest(String name) {
		Node node = getNode(name);
		for (Tree t : trees) {
			if (t.contains(node)) {
				return false;
			}
		}
		Tree t = new Tree(node);
		this.trees.add(t);
		return true;
	}

	public boolean addToForest(String nodeName, String parentName) {
		Node node = getNode(nodeName);
		Node parent = getNode(parentName);
		Tree treeP = null;
		Tree treeN = null;
		for (Tree t : trees) {
			if (t.contains(node)) {
				treeN = t;
			}
			if (t.contains(parent)) {
				treeP = t;
			}
		}
		if ((treeP == null) && (treeN == null)) {
			Tree t = new Tree(parent);
			trees.add(t);
			return t.addToTree(node, parent);
		}
		if ((treeP == null) || (treeN == null)) {
			Tree t = treeP == null ? treeN : treeP;
			return t.addToTree(node, parent);
		}
		if (!treeN.getRoot().equals(node)) {
			return false;
		}
		if (treeN == treeP) {
			return parent.children.contains(node);
		}
		trees.remove(treeN);
		return treeP.uniteTree(treeN, parent);
	}
	
	public boolean contains(String node) {
		return nodesMap.containsKey(node);
	}
	
	public Collection<Tree> getTrees() {
		return this.trees;
	}
	
	public void print() {
		for (Tree t : trees) {
			t.print();
		}
	}
	
	public int size() {
		return trees.size();
	}
	
	public Forest duplicate() {
		Forest f = new Forest();
		for (Tree t : this.trees) {
			Tree dup = t.duplicate();
			f.trees.add(dup);
			for (Node n : dup.nodes) {
				f.nodesMap.put(n.name, n);
			}
		}
		return f;
	}
	
	public static final String rootName = "__root__";
	public Tree asTree() {
		Node root = new Node(rootName);
		Tree res = new Tree(root);
		for (Tree t : trees) {
			root.children.add(t.getRoot());
		}
		res.nodes.addAll(nodesMap.values());
		return res;
	}
	
	public Collection<Node> getNodes() {
		return nodesMap.values();
	}
}
