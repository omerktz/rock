package vmm;

import java.util.Collection;
import java.util.Comparator;
import java.util.Map;
import java.util.Set;
//import java.util.TreeMap;
//import java.util.TreeSet;

import vmm.HierarchyBuilder.ComparableStringArray;

public class DistanceScorer implements PairScore {

	private static final int costReadWriteReplace = 5;
	private static double getEditCost(String t1,String t2) {
		if (t1.equals(t2)) {
			return 0;
		}
		if ((t1.startsWith("Read") || t1.startsWith("Write")) && (t2.startsWith("Read") || t2.startsWith("Write"))) {
			if (t1.substring(t1.indexOf("Offset")) == t2.substring(t2.indexOf("Offset"))) {
				return costReadWriteReplace;
			}
		}
		return 10;
	}
	private static class PairDouble extends Pair<Double> {
		public PairDouble(double f, double s) {
			query = f;
			reference = s;
		}
		public double first() { return query.doubleValue(); }
		public double second() { return reference.doubleValue(); }
	}
	private static PairDouble minPair(PairDouble pd1, PairDouble pd2) {
		if (pd1.first() < pd2.first()) {
			return pd1;
		}
		if (pd1.first() > pd2.first()) {
			return pd2;
		}
		if (pd1.second() < pd2.second()) {
			return pd1;
		}
		if (pd1.second() > pd2.second()) {
			return pd2;
		}
		return pd1;
	}
	private static int bool2int(boolean b) {
		if (b) {
			return 1;
		}
		return 0;
	}
	private static PairDouble damerau_levenshtein_distance(String[] seq1, String[] seq2) {
		PairDouble[] oneago = null;
		PairDouble[] twoago = null;
		int seq1firstIsOptional = bool2int(!seq1[0].startsWith("ReturnedFromFunction-"));
		int seq2firstIsOptional = bool2int(!seq2[0].startsWith("ReturnedFromFunction-"));
		PairDouble[] thisrow = new PairDouble[seq2.length + 1];
		for (int i = 0; i < seq2.length; i++) {
			thisrow[i] = new PairDouble((i+seq2firstIsOptional)*10, 0);
		}
		thisrow[seq2.length] = new PairDouble(0, 0);
		for (int x = 0; x < seq1.length; x++) {
			twoago = oneago;
			oneago = thisrow;
			thisrow = new PairDouble[seq2.length + 1];
			for (int i = 0; i < thisrow.length - 1; i++) {
				thisrow[i] = new PairDouble(0, 0);
			}
			thisrow[seq2.length] = new PairDouble((x+seq1firstIsOptional)*10, 0);
			for (int y = 0; y < seq2.length; y++) {
				PairDouble delcost = new PairDouble(oneago[y].first() + 10,oneago[y].second());
				PairDouble addcost = new PairDouble(thisrow[((seq2.length+1)+y-1)%(seq2.length+1)].first() + 10,thisrow[((seq2.length+1)+y-1)%(seq2.length+1)].second());
				PairDouble subcost = new PairDouble(oneago[((seq2.length+1)+y-1)%(seq2.length+1)].first() + getEditCost(seq1[x],seq2[y]),oneago[((seq2.length+1)+y-1)%(seq2.length+1)].second()+2*bool2int(seq1[x]!=seq2[y] && seq1[x].startsWith("CallFunction-") && seq2[y].startsWith("CallFunction-")));
				thisrow[y] = minPair(delcost, minPair(addcost, subcost));
				if (x > 0 && y > 0 && seq1[x] == seq2[((seq2.length+1)+y-1)%(seq2.length+1)] && seq1[((seq1.length+1)+x-1)%(seq1.length+1)] == seq2[y] && seq1[x] != seq2[y]) {
					thisrow[y] = minPair(thisrow[y], new PairDouble(twoago[((seq2.length+1)+y-2)%(seq2.length+1)].first() + 10,twoago[((seq2.length+1)+y-2)%(seq2.length+1)].second()));
				}
			}
		}
		return thisrow[seq2.length - 1];
	}

	@Override
	public double getScore(Pair<Set<String>> types, HierarchyBuilder dataSource) {
		Collection<ComparableStringArray> codesQuery = dataSource.getQueryCodes(types.query);
		Collection<ComparableStringArray> codesRef = dataSource.getRefCodes(types.reference);

		double score = 0;
		for (ComparableStringArray c : codesQuery) {
			PairDouble pd = null;
			for (ComparableStringArray p : codesRef) {
				if (pd == null) {
					pd = damerau_levenshtein_distance(c.data, p.data);
				} else {
					pd = minPair(pd, damerau_levenshtein_distance(c.data, p.data));
				}
			}
			score += pd.first();
		}
		return score;	
	}

	@Override
	public Comparator<Pair<Set<String>>> getComparator(Map<Pair<Set<String>>,Double> values) {
		return new ValueMin2MaxComparator(values);
	}
	@Override
	public String getFilenameAddition() {
		return ".Dist";
	}

	@Override
	public double getNormalizedScore(Map<Pair<Set<String>>, Double> pairs, Pair<Set<String>> p) {
		return pairs.get(p);
		//return new MeanNormalizer(pairs.values()).normalize(pairs.get(p));
	}
	@Override
	public String getName() {
		return "Distances";
	}
	
	@Override
	public double calcCombinedScore(double d1, double d2) {
		return d1+d2;
	}

	@Override
	public Comparator<Double> getValueComparator() {
		return new DoubleValueMin2MaxComparator();
	}
	
//	Map<String,Double> rootCache = new TreeMap<String,Double>();
//	@Override
//	public double getScoreAsRoot(String type, HierarchyBuilder dataSource) {
//		if (!rootCache.containsKey(type)) {
//			Pair<Set<String>> types = new Pair<Set<String>>();
//			types.query = new TreeSet<String>();
//			types.query.add(type);
//			types.reference = dataSource.allTypes;
//			rootCache.put(type, getScore(types, dataSource));
//		}
//		return rootCache.get(type);
//	}

	@Override
	public double getIntialValue() {
		return 0;
	}

}
