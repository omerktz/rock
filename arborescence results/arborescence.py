import statistics
import os
import sys

from  edmonds import getArborescence

def getChildren(t,h):
	children = set()
	for c in h[t]:
		children.add(c)
		children.update(getChildren(c,h))
	return children

def getMajorityVote(arbs):
	parentCounts = {}
	for a in arbs:
		for k in a.keys():
			if k not in parentCounts.keys():
				parentCounts[k] = {}
			if a[k] not in parentCounts[k].keys():
				parentCounts[k][a[k]] = 1
			else:
				parentCounts[k][a[k]] += 1
	pairs = []
	for k in parentCounts.keys():
		kpairs = []
		maxcount = max(parentCounts[k].values())
		for p in parentCounts[k].keys():
			if parentCounts[k][p] == maxcount:
				kpairs.append((k,p))
		pairs.append(kpairs)
	res = []
	for x in itertools.product(*pairs):
		h = dict(x)
		if h in arbs:
			res.append(h)
	return res

def getMaxNesting(a):
	def getMaxNestingInner(a,k,nesting):
		if k not in a.keys():
			return nesting
		return getMaxNestingInner(a,a[k],nesting+1)
	return max(map(lambda k: getMaxNestingInner(a,k,0), filter(lambda x: x not in a.values(), a.keys())))

def collectSuccessorsTree(filename):
	f = open(filename,'r')
	g = {}
	child = ''
	types = set()
	for line in f.readlines():
		if len(line.strip()) == 0:
			continue
		else:
			if line.strip() == '====================================================':
				continue
			else:
				if not line.startswith('\t'):
					child = line[:line.rfind('[')].strip()
					types.add(child)
				else:
					line = line.strip()
					if line[-1] not in map(lambda d:str(d),range(10)):
						line = line[:-1].strip()
					parent = line[:line.rfind('\t')]
					if child not in g.keys():
						g[child] = {}
					g[child][parent] = float(line[line.rfind('\t'):].strip())
	f.close()
	arborescences = [a for a in getArborescence(g)]
	newarborescences = getMajorityVote(arborescences)
	while len(newarborescences) != len(arborescences):
		arborescences = newarborescences
		newarborescences = getMajorityVote(arborescences)
	#if len(arborescences) > 1:
	#	nestings = map(lambda a: getMaxNesting(a), arborescences)
	#	minnesting = min(nestings)
	#	arborescences = map(lambda j: arborescences[j], filter(lambda i: nestings[i] == minnesting, range(len(arborescences))))
	i = 0
	for a in arborescences:
		if len(arborescences) > 1:
			revhierarchy = {}
			for k in a.keys():
				if a[k] not in revhierarchy.keys():
					revhierarchy[a[k]] = set()
				revhierarchy[a[k]].add(k)
			benchmarkname = os.path.basename(filename)
			with open(benchmarkname[:benchmarkname.find('.')]+'.hierarchy'+str(i),'w') as h:
				for k in sorted(filter(lambda k: k not in a.keys(), revhierarchy.keys())):
					def printTree(revhierarchy,current,depth,output):
						output.write('\t'*depth+current+'\n')
						if current in revhierarchy.keys():
							for x in sorted(revhierarchy[current]):
								printTree(revhierarchy,x,depth+1,output)
					printTree(revhierarchy,k,0,h)
				i += 1
		hierarchy = {}
		for t in types:
			hierarchy[t] = set()
		for n in a:
			hierarchy[a[n]].add(n)
		children = {}
		for t in hierarchy.keys():
			children[t] = getChildren(t,hierarchy)
		yield children

def collectGroundtruthTree(filename):
	f = open(filename,'r')
	lines =[]
	for line in f.readlines():
		if len(line.strip()) > 0:
			lines.append(line)
	lines = lines[1:]
	children = {}
	for i in range(0,len(lines)):
		childs = set()
		prefix = lines[i][0:lines[i].find(lines[i].strip()[0])]
		j = i+1
		while j < len(lines):
			jPrefix = lines[j][0:lines[j].find(lines[j].strip()[0])]
			if (len(jPrefix) > len(prefix)):
				childs.add(lines[j].strip())
			else:
				break
			j+=1
		children[lines[i].strip()] = childs
	f.close()
	return children

def collectSuccessorsList(filename):
	f = open(filename,'r')
	directChildren = {}
	child = ''
	types = set()
	for line in f.readlines():
		if len(line.strip()) == 0:
			continue
		else:
			if line.strip() == '====================================================':
				continue
			else:
				if not line.startswith('\t'):
					child = line[:line.rfind('[')].strip()
					types.add(child)
				else:
					line = line.strip()[:-1].strip()
					parent = line[:line.rfind('\t')]
					if parent not in directChildren.keys():
						directChildren[parent] = set()
					directChildren[parent].add(child)
	f.close()
	children = {}
	for i in types:
		childs = set()
		if i in directChildren.keys():
			newChilds = set()
			newChilds.update(directChildren[i])
			while len(newChilds) > 0:
				c = newChilds.pop()
				childs.add(c)
				if c in directChildren.keys():
					newChilds.update(directChildren[c])
					newChilds.difference_update(childs)
					newChilds.discard(i)
		children[i] = childs
	return children

def evalBenchmark(benchmark,filename,output,dir):
	strippedBenchmark = benchmark[:benchmark.rfind('.')]
	structural = collectSuccessorsList(os.path.join('.',strippedBenchmark,dir,benchmark+'.hierarchy'+filename+'.closest'))
	groundtruth = collectGroundtruthTree(os.path.join('.',strippedBenchmark,benchmark+'.groundtruth'))
	addedStruct = []
	missingStruct = []
	for t in groundtruth.keys():
		missingStruct.append(len(filter(lambda x: x not in structural[t], groundtruth[t])))
		addedStruct.append(len(filter(lambda x: x not in groundtruth[t], structural[t])))
	if len(missingStruct) == 0:
		missingStruct = [0,0]
	if len(addedStruct) == 0:
		addedStruct = [0,0]
	output.write(strippedBenchmark+' & '+str(len(groundtruth.keys()))+' & '+str(round(statistics.mean(missingStruct),2))+' & '+str(round(statistics.mean(addedStruct),2)))
	results = set()
	for constructed in collectSuccessorsTree(os.path.join('.',strippedBenchmark,dir,benchmark+'.hierarchy'+filename+'.closest')):
		addedModel = []
		missingModel = []
		for t in groundtruth.keys():
			missingModel.append(len(filter(lambda x: x not in constructed[t], groundtruth[t])))
			addedModel.append(len(filter(lambda x: x not in groundtruth[t], constructed[t])))
		if len(missingModel) == 0:
			missingModel = [0,0]
		if len(addedModel) == 0:
			addedModel = [0,0]
		res = (str(round(statistics.mean(missingModel),2)),str(round(statistics.mean(addedModel),2)))
		if res not in results:
			output.write(' | '+res[0]+' & '+res[1])
			results.add(res)
	output.write(' \\\\\n')

#if len(sys.argv) < 7:
#	print 'Usage: python '+sys.argv[0]+' <Q:true/false> <R:true/false> <DKL/JSD/JSDsqrt> <M/B/A> <1/10> <output>'
#	sys.exit(1)
if len(sys.argv) < 2:
	print 'Usage: python '+sys.argv[0]+' <output>'
	sys.exit(1)
def runTest(a,b,c,d,failed):
	print (a,b,c,d)
	filename = '.Q'+a+'.R'+b+'.'+'DKL'+'_'+c+'.'+d
	outputName = sys.argv[1]+'_'+a+'_'+b+'_'+c+'_'+d
	#if (c,d)in [('M','10'),('B','10'),('A','1')]:
	#	continue
	try:
		with open(outputName,'w') as output:
			with open('benchmarks.list','r') as benchmarks:
			#with open('benchmarksWithMultipleArborescences.list','r') as benchmarks:
				for l in benchmarks.readlines():
					l = l.strip()
					print '\t'+l
					if len(l) > 0:
						if os.path.exists(os.path.join('.',l[:l.rfind('.')])):
							evalBenchmark(l,filename,output,'DKL')
						else:
							output.write(l+'\n')
	except:
		os.remove(outputName)
		print '\t\tfailed!'
		failed.add((a,b,c,d))
import itertools
failed = set()
map(lambda x:runTest(*x),itertools.product(['false'],['true'],['M'],['10'],[failed]))
#map(lambda x:runTest(*x),itertools.product(['true','false'],['true','false'],['M','B','A'],['1','10'],[failed]))
print ''
for f in failed:
	print 'failed '+str(f)